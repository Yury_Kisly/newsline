package by.epam.newsapp.service.impl;


import by.epam.newsapp.dao.ICommentDAO;
import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class CommentServiceImpl implements ICommentService {

    @Autowired
    ICommentDAO commentDAO;  /*spring dependency injection*/

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #create(by.epam.newsapp.entity.Comment)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Long create(Comment comment) throws ServiceException {
        Long commentId;
        try {
            Timestamp creationDate = new Timestamp(System.currentTimeMillis());
            comment.setCreationDate(creationDate);
            commentId = commentDAO.create(comment);
        } catch (DAOException e) {
            throw new ServiceException("Comment can't be created", e);
        }
        return commentId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Comment readById(Long commentId) throws ServiceException {
        Comment comment;
        try {
            comment = commentDAO.readById(commentId);
        } catch (DAOException e) {
            throw new ServiceException("Comment can't be taken by id", e);
        }
        return comment;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #update(by.epam.newsapp.entity.Comment)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void update(Comment comment) throws ServiceException {
        try {
            Timestamp creationDate = new Timestamp(System.currentTimeMillis());
            comment.setCreationDate(creationDate);
            commentDAO.update(comment);
        } catch (DAOException e) {
            throw new ServiceException("Comment can't be updated", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void delete(Long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            throw new ServiceException("Comment can't be deleted", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.ICommentService #readByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.ICommentService
     */
    @Override
    public List<Comment> readByNewsId(Long newsId) throws ServiceException {
        List<Comment> list;
        try {
            list = commentDAO.readByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException("List of comments can't be taken by news.xml id", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.ICommentService #readAll()} )}
     * @see by.epam.newsapp.service.ICommentService
     */
    @Override
    public List<Comment> readAll() throws ServiceException {
        List<Comment> list;
        try {
            list = commentDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException("List of comments can't be taken", e);
        }
        return list;
    }

    @Override
    public void deleteByNewsId(Long newsId) throws ServiceException {
        try {
            commentDAO.deleteByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException("Comment can't be deleted by news id", e);
        }
    }


}
