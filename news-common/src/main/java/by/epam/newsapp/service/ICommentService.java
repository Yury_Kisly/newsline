package by.epam.newsapp.service;


import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.ServiceException;

import java.util.List;

/**
 * The interface ICommentService
 */
public interface ICommentService extends IService<Comment, Long> {

    /**
     * method gets a list of news' comments
     * @param newsId news id
     * @return list of all comments by news id
     * @throws ServiceException
     */
    List<Comment> readByNewsId(Long newsId) throws ServiceException;

    /**
     * method gets a list of all comments from DB
     * @return list of all comments
     * @throws ServiceException
     */
    List<Comment> readAll() throws ServiceException;

    /**
     * method delete all news' comments
     * @param newsId news id
     * @throws ServiceException
     */
    void deleteByNewsId(Long newsId) throws ServiceException;

}
