package by.epam.newsapp.service;

import by.epam.newsapp.exception.ServiceException;

/**
 * The interface ICompletedTagService
 */
public interface ICompletedTagService {

    /**
     * method delete tag from news_tag and tag tables
     * @param tagId
     * @throws ServiceException
     */
    void deleteWholeTag(Long tagId) throws ServiceException;
}
