package by.epam.newsapp.service;


import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.ServiceException;

/**
 * The interface IUserService
 */
public interface IUserService extends IService<User, Long> {

    /**
     * checks if user exists in a db
     * @param login
     * @param password
     * @return checked user or null
     * @throws ServiceException
     */
    User checkUser(String login, String password) throws ServiceException;

}
