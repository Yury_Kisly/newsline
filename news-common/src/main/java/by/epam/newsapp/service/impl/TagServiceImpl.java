package by.epam.newsapp.service.impl;


import by.epam.newsapp.dao.ITagDAO;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The class TagServiceImpl
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class TagServiceImpl implements ITagService {

    @Autowired
    private ITagDAO tagDAO; /*spring dependency injection*/

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #create(by.epam.newsapp.entity.Tag)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Long create(Tag tag) throws ServiceException {
        Long tagId;
        try {
            tagId = tagDAO.create(tag);
        } catch (DAOException e) {
            throw new ServiceException("Tag can't be added", e);
        }
        return tagId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Tag readById(Long tagId) throws ServiceException {
        Tag tag;
        try {
            tag = tagDAO.readById(tagId);
        } catch (DAOException e) {
            throw new ServiceException("Tag can't be taken by id.", e);
        }
        return tag;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #update(by.epam.newsapp.entity.Tag)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void update(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            throw new ServiceException("Tag can't be updated", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void delete(Long tagId) throws ServiceException {
        try {
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            throw new ServiceException("Tag can't be deleted", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.ITagService #readAll()} )}
     * @see by.epam.newsapp.service.ITagService
     */
    @Override
    public List<Tag> readAll() throws ServiceException {
        List<Tag> list;
        try {
            list = tagDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException("List of tags can't be taken", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.ITagService #readByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.ITagService
     */
    @Override
    public List<Tag> readByNewsId(Long newsId) throws ServiceException {
        List<Tag> list;
        try {
            list = tagDAO.readByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException("List of tags can't be taken by news.xml id", e);
        }
        return list;
    }

    @Override
    public void deleteNewsTagByTagId(Long tagId) throws ServiceException {
        try {
            tagDAO.deleteNewsTagByTagId(tagId);
        } catch (DAOException e) {
            throw new ServiceException("ServiceException while the tag was deleted from tag_news by tag id", e);
        }
    }
}
