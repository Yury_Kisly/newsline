package by.epam.newsapp.service;

import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.ServiceException;

/**
 * The interface IDAO contains CRUD methods for DB
 * @param <E>   generic type
 * @param <T>   generic type
 */
public interface IService<E, T> {
    /**
     * method adds an element to database
     * @param element
     * @return element id
     * @throws ServiceException
     */
    T create(E element) throws ServiceException;

    /**
     * method takes current element by element id
     * @param elemId
     * @return an entity
     * @throws ServiceException
     */
    E readById(T elemId) throws ServiceException;

    /**
     * method updates element
     * @param element
     * @throws ServiceException
     */
    void update (E element) throws ServiceException;

    /**
     * method deletes current element by element id
     * @param elemId
     * @throws ServiceException
     */
    void delete(T elemId) throws ServiceException;

}
