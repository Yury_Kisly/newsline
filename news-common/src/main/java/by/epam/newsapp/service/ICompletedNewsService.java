package by.epam.newsapp.service;

import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.ServiceException;

import java.util.List;

/**
 * The interface ICompletedNewsService
 */
public interface ICompletedNewsService {

    /**
     * method creates a news with authors and tags
     * @param news
     * @param authorIdList
     * @param tagIdList
     * @throws ServiceException
     */
    Long createWholeNews(News news, List<Long> authorIdList, List<Long> tagIdList) throws ServiceException;

    /**
     * method takes a news with authors, tags and comments
     * @param newsId
     * @return data transfer object
     * @throws ServiceException
     */
    NewsTO readWholeNews(Long newsId) throws ServiceException;

    /**
     * method updates news
     * @param news
     * @param authorIdList
     * @param tagIdList
     * @throws ServiceException
     */
    void updateWholeNews(News news, List<Long> authorIdList, List<Long> tagIdList) throws ServiceException;

    /**
     * method deletes news with all its data
     * @param newsId
     * @throws ServiceException
     */
    void deleteWholeNews(Long[] newsId) throws ServiceException;
}
