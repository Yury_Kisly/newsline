package by.epam.newsapp.service;


import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.ServiceException;

import java.util.List;

/**
 * The interface ITagService
 */
public interface ITagService extends IService<Tag, Long> {

    /**
     * method gets a list of all tags from DB
     * @return list of all tags
     * @throws ServiceException
     */
    List<Tag> readAll() throws ServiceException;

    /**
     * method gets a list of news' tags
     * @param newsId news id
     * @return list of tags by news id
     * @throws ServiceException
     */
    List<Tag> readByNewsId(Long newsId) throws ServiceException;

    /**
     * method deletes a tag from news_tag table by tag id
     * @param tagId tag id
     * @throws ServiceException
     */
    void deleteNewsTagByTagId(Long tagId) throws ServiceException;
}
