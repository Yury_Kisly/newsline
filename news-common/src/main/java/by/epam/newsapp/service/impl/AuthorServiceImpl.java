package by.epam.newsapp.service.impl;

import by.epam.newsapp.dao.IAuthorDAO;
import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * The class AuthorServiceImpl
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class AuthorServiceImpl implements IAuthorService {

    @Autowired
    private IAuthorDAO authorDAO;   /*spring dependency injection*/

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #create(by.epam.newsapp.entity.Author)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Long create(Author author) throws ServiceException {
        Long authorId;
        try {
            authorId = authorDAO.create(author);
        } catch (DAOException e) {
            throw new ServiceException("Author can't be created.", e);
        }
        return authorId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Author readById(Long authorId) throws ServiceException {
        Author author;
        try {
            author = authorDAO.readById(authorId);
        } catch (DAOException e) {
            throw new ServiceException("Author can't be taken by id", e);
        }
        return author;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #update(by.epam.newsapp.entity.Author)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void update(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            throw new ServiceException("Author can't be apdated", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void delete(Long authorId) throws ServiceException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IAuthorService #readAll()} )}
     * @see by.epam.newsapp.service.IAuthorService
     */
    @Override
    public List<Author> readAll() throws ServiceException {
        List<Author> list;
        try {
            list = authorDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException("List of authors can't be taken.", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IAuthorService #readByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IAuthorService
     */
    @Override
    public List<Author> readByNewsId(Long newsId) throws ServiceException {
        List<Author> list;
        try {
            list = authorDAO.readByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException("List of authors can't be taken by news.xml id.", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IAuthorService #authorExpire(by.epam.newsapp.entity.Author)} )}
     * @see by.epam.newsapp.service.IAuthorService
     */
    @Override
    public void authorExpire(Long authorId) throws ServiceException {
        try {
            authorDAO.authorExpire(authorId);
        } catch (DAOException e) {
            throw new ServiceException("Author can't be expired.", e);
        }
    }

}
