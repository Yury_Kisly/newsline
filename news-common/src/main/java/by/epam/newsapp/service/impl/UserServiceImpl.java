package by.epam.newsapp.service.impl;

import by.epam.newsapp.dao.IUserDAO;
import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The class UserServiceImpl
 */

@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class UserServiceImpl implements IUserService {

    @Autowired
    private IUserDAO userDAO;

    /**
     * Implementation of method {@link by.epam.newsapp.service.IUserService #checkUser(java.lang.String, java.lang.String)} )}
     * @see by.epam.newsapp.service.IUserService
     */
    @Override
    public User checkUser(String login, String password) throws ServiceException {
        User user;
        try {
            user = userDAO.checkUser(login, password);
        } catch (DAOException exp) {
            throw new ServiceException("User can't be checked", exp);
        }
        return user;
    }

    @Override
    public Long create(User element) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public User readById(Long elemId) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(User element) throws ServiceException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Long elemId) throws ServiceException {
        throw new UnsupportedOperationException();
    }


}
