package by.epam.newsapp.service;

import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.ServiceException;

import java.util.List;

/**
 * The interface IAuthorService
 */
public interface IAuthorService extends IService<Author, Long> {

    /**
     * method gets a list of all authors from DB
     * @return list of all authors
     * @throws ServiceException
     */
    List<Author> readAll() throws ServiceException;

    /**
     * gets a list of news' authors
     * @param newsId news id
     * @return list of authors by news id
     * @throws ServiceException
     */
    List<Author> readByNewsId(Long newsId) throws ServiceException;

    /**
     * method changes author expire date for a current
     * @param authorId
     * @throws ServiceException
     */
    void authorExpire(Long authorId) throws ServiceException;

}
