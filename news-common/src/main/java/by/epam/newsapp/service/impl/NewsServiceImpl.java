package by.epam.newsapp.service.impl;

import by.epam.newsapp.dao.INewsDAO;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * The class NewsServiceImpl
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class NewsServiceImpl implements INewsService {

    @Autowired
    private INewsDAO newsDAO; /*spring dependency injection*/

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #create(by.epam.newsapp.entity.News)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public Long create(News news) throws ServiceException {
        Long newsId;
        try {
            news.setCreationDate(new Timestamp(System.currentTimeMillis()));
            news.setModificationDate(new Date(System.currentTimeMillis()));
            newsId = newsDAO.create(news);
        } catch (DAOException e) {
            throw new ServiceException("News can't be added", e);
        }
        return newsId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public News readById(Long newsId) throws ServiceException {
        News news;
        try {
            news = newsDAO.readById(newsId);
        } catch (DAOException e) {
            throw new ServiceException("News can't be taken by id.", e);
        }
        return news;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #update(by.epam.newsapp.entity.News)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void update(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            throw new ServiceException("News can't be updated", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.IService #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.service.IService
     */
    @Override
    public void delete(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            throw new ServiceException("News can't be deleted", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.INewsService #readAll()} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public List<News> readAll() throws ServiceException {
        List<News> list;
        try {
            list = newsDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException("List of news can't be taken", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.INewsService #readByAuthorId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public List<News> readByAuthorId(Long authorId) throws ServiceException {
        List<News> list;
        try {
            list = newsDAO.readByAuthorId(authorId);
        } catch (DAOException e) {
            throw new ServiceException("List of news can't be taken by author id", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.INewsService #readByTagId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public List<News> readByTagId(Long tagId) throws ServiceException {
        List<News> list;
        try {
            list = newsDAO.readByTagId(tagId);
        } catch (DAOException e) {
            throw new ServiceException("List of news can't be taken by tag id", e);
        }
        return list;
    }

    /**
     * method adds the list of authors to the News_Author table
     *
     * Implementation of method {@link by.epam.newsapp.service.INewsService #addNewsAuthorList(java.lang.Long, java.util.List)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public void addNewsAuthorList(Long newsId, List<Long> authorIdList) throws ServiceException {
        try {
            newsDAO.addNewsAuthorList(newsId, authorIdList);
        } catch (DAOException e) {
            throw new ServiceException("List of authors can't be added to news", e);
        }
    }

    /**
     * method adds the list of tags to the News_Tag table
     *
     * Implementation of method {@link by.epam.newsapp.service.INewsService #addNewsTagList(java.lang.Long, java.util.List)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public void addNewsTagList(Long newsId, List<Long> tagIdList) throws ServiceException {
        try {
            newsDAO.addNewsTagList(newsId, tagIdList);
        } catch (DAOException e) {
            throw new ServiceException("List of authors can't be added to news", e);
        }
    }

    /**
     * method deletes all authors by news id from the News_Author table
     *
     * Implementation of method {@link by.epam.newsapp.service.INewsService #deleteNewsAuthorByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public void deleteNewsAuthorByNewsId(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsAuthorByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException("ServiceException while a news_author.xml was deleted by news.xml id", e);
        }
    }

    /**
     * method deletes all tags by news id from the News_Tag table
     *
     * Implementation of method {@link by.epam.newsapp.service.INewsService #deleteNewsTagByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public void deleteNewsTagByNewsId(Long newsId) throws ServiceException {
        try {
            newsDAO.deleteNewsTagByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException("ServiceException while a news_tag.xml was deleted by news.xml id", e);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.INewsService #takeBySearchCriteria(by.epam.newsapp.entity.SearchCriteria)} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public List<Long> takeBySearchCriteria(SearchCriteria criteria) throws ServiceException {
        List<Long> list;
        try {
            list = newsDAO.takeBySearchCriteria(criteria);
        } catch (DAOException e) {
            throw new ServiceException("Service exception while a list of news ids was taken by search criteria", e);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.service.INewsService #countAllNews()} )}
     * @see by.epam.newsapp.service.INewsService
     */
    @Override
    public long countAllNews() throws ServiceException {
        long value;
        try {
            value = newsDAO.countAllNews();
        } catch (DAOException e) {
            throw new ServiceException("Service exception while all news were counted", e);
        }
        return value;
    }
}
