package by.epam.newsapp.service.impl;


import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICompletedTagService;
import by.epam.newsapp.service.ITagService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CompletedTagServiceImpl implements ICompletedTagService {
//    private final static Logger LOGGER = Logger.getLogger(CompletedTagServiceImpl.class);

    @Autowired
    ITagService tagService;

    /**
     * Implementation of method {@link ICompletedTagService #deleteWholeTag(java.lang.Long)} )}
     * @see ICompletedTagService
     */
    @Override
    public void deleteWholeTag(Long tagId) throws ServiceException {
//        try {
            tagService.deleteNewsTagByTagId(tagId);         //is empty for HIBERNATE
            tagService.delete(tagId);
//        if (true) {
//            throw new ServiceException("Papam");
//        }
//        } catch (ServiceException exp) {
//            LOGGER.error("Whole tag can't be deleted", exp);
//            throw exp;
//        }
    }
}
