package by.epam.newsapp.service.impl;

import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
public class CompletedNewsServiceImpl implements ICompletedNewsService {
//    private final static Logger LOGGER = Logger.getLogger(CompletedNewsServiceImpl.class);

    /*spring dependency injection*/
    @Autowired
    private INewsService newsService;
    @Autowired
    private ITagService tagService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IAuthorService authorService;

    /**
     * Implementation of method {@link ICompletedNewsService #createWholeNews(by.epam.newsapp.entity.News,
     *                                                                          java.util.List, java.util.List)} )}
     * @see ICompletedNewsService
     */
    @Override
    public Long createWholeNews(News news, List<Long> authorIdList, List<Long> tagIdList) throws ServiceException {
        Long newsId = null;
        try {
            newsId = newsService.create(news);
            if (authorIdList != null && !authorIdList.isEmpty()) {
                newsService.addNewsAuthorList(newsId, authorIdList);
            }
            if (tagIdList != null && !tagIdList.isEmpty()) {
                newsService.addNewsTagList(newsId, tagIdList);
            }
        } catch (ServiceException exp) {
//            LOGGER.error("Whole news can't be created", exp);
            throw exp;
        }
        return newsId;
    }

    /**
     * Implementation of method {@link ICompletedNewsService #readWholeNews(java.lang.Long)} )}
     * @see ICompletedNewsService
     */
    @Override
    public NewsTO readWholeNews(Long newsId) throws ServiceException {
        NewsTO newsTO  = null;
        try {
            News news = newsService.readById(newsId);
            //don't use other service if news doesn't exists
            if (news != null) {
                newsTO = new NewsTO();
                newsTO.setNews(news);
                newsTO.setTagList(tagService.readByNewsId(newsId));
                newsTO.setAuthorList(authorService.readByNewsId(newsId));
                newsTO.setCommentList(commentService.readByNewsId(newsId));
            }
        } catch (ServiceException exp) {
//            LOGGER.error("Whole news can't be taken", exp);
            throw exp;
        }
        return newsTO;
    }

    /**
     * Implementation of method {@link ICompletedNewsService #deleteWholeNews(java.lang.Long)} )}
     * @see ICompletedNewsService
     */
    @Override
    public void deleteWholeNews(Long[] newsId) throws ServiceException {

        for(Long id : newsId) {
            try {
                commentService.deleteByNewsId(id);          //is empty for HIBERNATE
                newsService.deleteNewsAuthorByNewsId(id);
                newsService.deleteNewsTagByNewsId(id);
                newsService.delete(id);
            } catch (ServiceException exp) {
//            LOGGER.error("Whole news can't be deleted", exp);
                throw exp;
            }
        }

    }

    /**
     * Implementation of method {@link ICompletedNewsService #createWholeNews(by.epam.newsapp.entity.News,
     *                                                                                 java.util.List, java.util.List)} )}
     * @see ICompletedNewsService
     */
    @Override
    public void updateWholeNews(News news, List<Long> authorIdList, List<Long> tagIdList) throws ServiceException {
        Long newsId = news.getNewsId();
        System.out.println("NEWS UPDATE=" + news);
        System.out.println(news.getAuthorSet() == null);
        System.out.println(news.getTagSet() == null);
        System.out.println(news.getCommentSet() == null);
        try {
            newsService.deleteNewsAuthorByNewsId(newsId);
            newsService.deleteNewsTagByNewsId(newsId);
            newsService.update(news);
            if (authorIdList != null && !authorIdList.isEmpty()) {
                newsService.addNewsAuthorList(newsId, authorIdList);
            }
            if (tagIdList != null && !tagIdList.isEmpty()) {
                newsService.addNewsTagList(newsId, tagIdList);
            }
        } catch (ServiceException exp) {
//            LOGGER.error("Whole news can't be updated", exp);
            throw exp;
        }
    }
}
