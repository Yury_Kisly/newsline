package by.epam.newsapp.service;

import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.ServiceException;

import java.util.List;

/**
 * Created by Yury_Kisly on 4/28/2016.
 */
public interface INewsService extends IService<News, Long>{
    /**
     * method gets a list of all news from DB
     * @return list of all news
     * @throws ServiceException
     */
    List<News> readAll() throws ServiceException;

    /**
     * method gets a list of news where author is mentioned
     * @param authorId author id
     * @return list of all news by author id
     * @throws ServiceException
     */
    List<News> readByAuthorId(Long authorId) throws ServiceException;

    /**
     * method gets a list of news where tag is mentioned
     * @param tagId tag id
     * @return list of all news by tag id
     * @throws ServiceException
     */
    List<News> readByTagId(Long tagId) throws ServiceException;

    /**
     * method add authors for the news
     * @param newsId news id
     * @param authorId author id
     * @throws ServiceException
     */
    void addNewsAuthorList(Long newsId, List<Long> authorId) throws ServiceException;

    /**
     * method add tags for the news
     * @param newsId news id
     * @param tagId tag id
     * @throws ServiceException
     */
    void addNewsTagList(Long newsId, List<Long> tagId) throws ServiceException;

    /**
     * method deletes all news' authors by news id
     * @param newsId news id
     * @throws ServiceException
     */
    void deleteNewsAuthorByNewsId(Long newsId) throws ServiceException;

    /**
     * method deletes all news' tags by news id
     * @param newsId news id
     * @throws ServiceException
     */
    void deleteNewsTagByNewsId(Long newsId) throws ServiceException;

    /**
     * method gets a list of news matched the search criteria
     * @param criteria an entity contains a list of tags and authors
     * @return a list of news
     * @throws ServiceException
     */
    List<Long> takeBySearchCriteria(SearchCriteria criteria) throws ServiceException;

    /**
     * method counts all news in DB
     * @return the number of news
     * @throws ServiceException
     */
    long countAllNews() throws ServiceException;
}
