package by.epam.newsapp.exception;

public class ServiceException extends Exception{
    public ServiceException(String str) { super(str); }
    public ServiceException(Exception exp) { super(exp); }
    public ServiceException(String str, Exception exp) { super(str, exp); }
}
