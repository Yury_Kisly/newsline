package by.epam.newsapp.exception;

public class DAOException extends Exception {
    public DAOException(String str) { super(str); }
    public DAOException(Exception exp) { super(exp); }
    public DAOException(String str, Exception exp) { super(str, exp); }
}
