package by.epam.newsapp.entity;

import java.util.ArrayList;
import java.util.List;

//transfer object for news adding
public class CreateNewsTO {
    private News news = new News();
    private List<Long> tagsId = new ArrayList<>();
    private List<Long> authorsId = new ArrayList<>();

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Long> getTagsId() {
        return tagsId;
    }

    public void setTagsId(List<Long> tagsId) {
        this.tagsId = tagsId;
    }

    public List<Long> getAuthorsId() {
        return authorsId;
    }

    public void setAuthorsId(List<Long> authorsId) {
        this.authorsId = authorsId;
    }
}
