package by.epam.newsapp.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "NEWS")
@SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ", allocationSize=1)
public class News implements Serializable {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQ")
    @Column(name = "NEWS_ID")
    private Long newsId;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "SHORT_TEXT")
    private String shortText;

    @Column(name = "FULL_TEXT")
    private String fullText;

    @Column(name = "CREATION_DATE")
    private Timestamp creationDate;

    @Column(name = "MODIFICATION_DATE")
    private Date modificationDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="NEWS_TAG", joinColumns = @JoinColumn(name = "NEWS_ID"),
            inverseJoinColumns =@JoinColumn(name="TAG_ID" ))
    private Set<Tag> tagSet;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="NEWS_AUTHOR", joinColumns = @JoinColumn(name = "NEWS_ID"),
            inverseJoinColumns =@JoinColumn(name="AUTHOR_ID" ))
    private Set<Author> authorSet;  //many-to-many

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "news")
    private Set<Comment> commentSet;//one-to-many

    @Version
    @Column(name = "VERSION")
    private Long version;           //OptimisticLock


    public Long getNewsId() {
        return newsId;
    }

    public void setNewsId(Long newsID) {
        this.newsId = newsID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Set<Tag> getTagSet() {
        return tagSet;
    }

    public void setTagSet(Set<Tag> tagSet) {
        this.tagSet = tagSet;
    }

    public Set<Author> getAuthorSet() {
        return authorSet;
    }

    public void setAuthorSet(Set<Author> authorSet) {
        this.authorSet = authorSet;
    }

    public Set<Comment> getCommentSet() {
        return commentSet;
    }

    public void setCommentSet(Set<Comment> commentSet) {
        this.commentSet = commentSet;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }

        News news = (News) o;

        if (newsId != null ? !newsId.equals(news.getNewsId()) : news.getNewsId() != null) { return false; }
        if (title != null ? !title.equals(news.getTitle()) : news.getTitle() != null) { return false; }
        if (shortText != null ? !shortText.equals(news.getShortText()) : news.getShortText() != null) {
            return false; }
        if (fullText != null ? !fullText.equals(news.getFullText()) : news.getFullText() != null) {
            return false; }
        if (creationDate != null ? !creationDate.equals(news.getCreationDate()) : news.getCreationDate() != null) {
            return false; }
        return !(modificationDate != null ? !modificationDate.equals(news.getModificationDate()) :
                                                                                news.getModificationDate() != null);

    }

    @Override
    public int hashCode() {
        int result = (newsId != null ? newsId.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("News{");
        sb.append("newsId=").append(newsId);
        sb.append(", title='").append(title).append('\'');
        sb.append(", shortText='").append(shortText).append('\'');
        sb.append(", fullText='").append(fullText).append('\'');
        sb.append(", creationDate=").append(creationDate);
        sb.append(", modificationDate=").append(modificationDate);
        sb.append('}');
        return sb.toString();
    }
}
