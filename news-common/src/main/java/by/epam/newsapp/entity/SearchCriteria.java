package by.epam.newsapp.entity;


import java.util.ArrayList;
import java.util.List;

public class SearchCriteria {
    private List<Long> tagIdList = new ArrayList<>();
    private List<Long> authorIdList = new ArrayList<>();

    public List<Long> getTagIdList() {
        return tagIdList;
    }

    public void setTagId(Long tagId) {
        tagIdList.add(tagId);
    }

    public List<Long> getAuthorIdList() {
        return authorIdList;
    }

    public void setAuthorId(Long authorId) {
        authorIdList.add(authorId);
    }
}
