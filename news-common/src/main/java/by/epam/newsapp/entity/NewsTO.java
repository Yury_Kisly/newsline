package by.epam.newsapp.entity;

import java.util.ArrayList;
import java.util.List;


public class NewsTO {
    private News news;
    private List<Comment> commentList = new ArrayList<>();
    private List<Author> authorList = new ArrayList<>();
    private List<Tag> tagList = new ArrayList<>();



    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public List<Comment> getCommentList() {
        return commentList;
    }

    public void setCommentList(List<Comment> commentList) {
        this.commentList = commentList;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public List<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(List<Tag> tagList) {
        this.tagList = tagList;
    }




}
