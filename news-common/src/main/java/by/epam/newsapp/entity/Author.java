package by.epam.newsapp.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="AUTHOR")
@SequenceGenerator(name = "AUTHOR_SEQ", sequenceName = "AUTHOR_SEQ", allocationSize=1)
public class Author implements Serializable {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_SEQ")
    @Column(name = "AUTHOR_ID")
    private Long authorId;

    @Column(name = "AUTHOR_NAME")
    private String authorName;

    @Column(name = "EXPIRED")
    private Timestamp expired;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "NEWS_AUTHOR", joinColumns = @JoinColumn(name = "AUTHOR_ID"),
            inverseJoinColumns =@JoinColumn(name="NEWS_ID" ))
    private Set<News> newsSet;

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Timestamp getExpired() {
        return expired;
    }

    public void setExpired(Timestamp expired) {
        this.expired = expired;
    }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public void setNewsSet(Set<News> newsSet) {
        this.newsSet = newsSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || this.getClass() != o.getClass()) { return false; }

        Author author = (Author) o;

        if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) { return false; }
        if (authorName != null ? !authorName.equals(author.getAuthorName()) : author.getAuthorName() != null) {
            return false; }
        return !(expired != null ? !expired.equals(author.getExpired()) : author.getExpired() != null);
    }

    @Override
    public int hashCode() {
        int result = authorId != null ? authorId.hashCode() : 0;
        result = 31 * result + (authorName != null ? authorName.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Author{");
        sb.append("authorID=").append(authorId);
        sb.append(", authorName='").append(authorName).append('\'');
        sb.append(", expired=").append(expired);
        sb.append('}');
        return sb.toString();
    }
}
