package by.epam.newsapp.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="TAG")
@SequenceGenerator(name = "TAG_SEQ", sequenceName = "TAG_SEQ", allocationSize=1)
public class Tag implements Serializable {
    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
    @Column(name = "TAG_ID")
    private Long tagId;

    @Column(name = "TAG_NAME")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="NEWS_TAG", joinColumns = @JoinColumn(name = "TAG_ID"),
            inverseJoinColumns =@JoinColumn(name="NEWS_ID" ))
    private Set<News> newsSet;


    public Long getTagId() { return tagId; }

    public void setTagId(Long tagID) { this.tagId = tagID; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Set<News> getNewsSet() {
        return newsSet;
    }

    public void setNewsSet(Set<News> newsSet) {
        this.newsSet = newsSet;
    }



    @Override
    public boolean equals(Object o) {
        if(this == o) { return true; }
        if (o == null || this.getClass() != o.getClass()) { return false; }

        Tag ref = (Tag)o;

        if (tagId != null ? !tagId.equals(ref.getTagId()) : ref.getTagId() != null) { return false; }
        return !(name != null ? !name.equals(ref.getName()) : ref.getName() != null);

    }

    @Override
    public int hashCode() {
        int result = (tagId != null ? tagId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Tag{");
        sb.append("tagID=").append(tagId);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
