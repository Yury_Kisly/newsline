package by.epam.newsapp.dao.impl.hibernate;

import by.epam.newsapp.dao.IUserDAO;
import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public class UserDAOImpl implements IUserDAO {

    private static final String HQL_CHECK_USER = "select u from User u where login =:login and password =:password";

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public User checkUser(String login, String password) throws DAOException {
        User user;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createQuery(HQL_CHECK_USER);
            query.setParameter("login", login);
            query.setParameter("password", password);
            user = (User)query.uniqueResult();
        }catch (HibernateException e) {
            throw new DAOException("SQLException while user was checking.", e );
        }
        return user;
    }

    @Override
    public Long create(User user) throws DAOException {
        Serializable id;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            id = currentSession.save(user);
            currentSession.flush();
        }catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a user was created: " + user, e);
        }
        return (Long)id;
    }

    @Override
    public User readById(Long userId) throws DAOException {
        User user;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            user = (User)currentSession.get(User.class, userId);
        }catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a user was taken by id: " + userId, e);
        }
        return user;
    }

    @Override
    public void update(User user) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            currentSession.update(user);
            currentSession.flush();
        }catch (HibernateException e) {
            throw new DAOException("SQLException while a user was updated by user: " + user, e);
        }
    }

    @Override
    public void delete(Long userId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            User user = (User)currentSession.createCriteria(User.class).add(Restrictions.idEq(userId)).uniqueResult();
            if (user != null) {
                currentSession.delete(user);
//                currentSession.flush();
            }
        }catch (HibernateException e) {
            throw new DAOException("SQLException while user was deleted by id: " + userId, e );
        }
    }
}
