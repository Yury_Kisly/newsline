package by.epam.newsapp.dao.impl.JDBC;


import by.epam.newsapp.dao.INewsDAO;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//@Transactional(propagation = Propagation.MANDATORY)
public class NewsDAOImpl implements INewsDAO {
    private static final String SQL_CREATE_NEWS = "insert into NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) values (NEWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
    private static final String SQL_READ_NEWS_BY_ID = "select NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE from NEWS where NEWS_ID=?";
    private static final String SQL_READ_NEWS_BY_AUTHOR_ID = "select NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE from NEWS join NEWS_AUTHOR on NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID and NEWS_AUTHOR.AUTHOR_ID=?";
    private static final String SQL_READ_NEWS_BY_TAG_ID = "select NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE from NEWS join NEWS_TAG on NEWS.NEWS_ID = NEWS_TAG.NEWS_ID and NEWS_TAG.TAG_ID=?";
    private static final String SQL_READ_ALL_NEWS = "select NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE from NEWS";
    private static final String SQL_UPDATE_NEWS = "update NEWS set TITLE=?, SHORT_TEXT=?, FULL_TEXT=?, CREATION_DATE=?, MODIFICATION_DATE=? where NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS = "delete from NEWS where NEWS_ID=?";
    private static final String SQL_ADD_NEWS_AUTHOR = "insert into NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) values (?, ?)";
    private static final String SQL_ADD_NEWS_TAG = "insert into NEWS_TAG (NEWS_ID, TAG_ID) values (?, ?)";
    private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "delete from NEWS_AUTHOR where NEWS_ID=?";
    private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "delete from NEWS_TAG where NEWS_ID=?";
    private static final String SQL_COUNT_ALL_NEWS = "select count(NEWS_ID) from NEWS";

    /*Search criteria*/
    /*if no search criteria*/
    private static final String SQL_COMMON = "select distinct NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE from NEWS";
    /*if search criteria with authors list*/
    private static final String SQL_AUTHOR_QUEUE = " join NEWS_AUTHOR on (NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID) where";
    /*if search criteria with tags list*/
    private static final String SQL_TAG_QUEUE = " join NEWS_TAG on (NEWS.NEWS_ID=NEWS_TAG.NEWS_ID) where";
    /*if search criteria with tags and authors lists*/
    private static final String SQL_TAG_AUTHOR_QUEUE = " join NEWS_AUTHOR on (NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID) join NEWS_TAG on (NEWS.NEWS_ID=NEWS_TAG.NEWS_ID) where";

    private static final String SQL_PART_ONE = "select SC.NEWS_ID from ( ";
    private static final String SQL_PART_TWO = " ) SC left join COMMENTS on SC.NEWS_ID=COMMENTS.NEWS_ID group by SC.NEWS_ID, SC.CREATION_DATE order by count(COMMENTS.COMMENT_ID) desc, SC.CREATION_DATE desc";


    private static final String NEWS_ID = "NEWS_ID";
    private static final String TITLE = "TITLE";
    private static final String SHORT_TEXT = "SHORT_TEXT";
    private static final String FULL_TEXT = "FULL_TEXT";
    private static final String CREATION_DATE = "CREATION_DATE";
    private static final String MODIFICATION_DATE = "MODIFICATION_DATE";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #create(by.epam.newsapp.entity.News)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Long create(News news) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String[] column = {NEWS_ID};    //for Oracle to prevent empty ResultSet from getGeneratedKeys()
        Long newsId = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_NEWS, column)) {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                newsId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news was created: " + news, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return newsId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public News readById(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        News news = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_NEWS_BY_ID)) {
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                news = new News();
                news.setNewsId(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news.xml was taken by id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return news;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #readByAuthorId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public List<News> readByAuthorId(Long authorId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<News> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_NEWS_BY_AUTHOR_ID)) {
            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of  news were taken by author id: " + authorId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #readByTagId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public List<News> readByTagId(Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<News> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_NEWS_BY_TAG_ID)) {
            preparedStatement.setLong(1, tagId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                News news = new News();
                news.setNewsId(resultSet.getLong(NEWS_ID));
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of news were taken by tag id: " + tagId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #readAll()} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public List<News> readAll() throws DAOException{
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<News> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_READ_ALL_NEWS);
            while(resultSet.next()) {
                News news = new News();
                news.setTitle(resultSet.getString(TITLE));
                news.setShortText(resultSet.getString(SHORT_TEXT));
                news.setFullText(resultSet.getString(FULL_TEXT));
                news.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                news.setModificationDate(resultSet.getDate(MODIFICATION_DATE));
                list.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of news.xml was taken", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #update(by.epam.newsapp.entity.News)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void update(News news) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_NEWS)) {
            preparedStatement.setString(1, news.getTitle());
            preparedStatement.setString(2, news.getShortText());
            preparedStatement.setString(3, news.getFullText());
            preparedStatement.setTimestamp(4, news.getCreationDate());
            preparedStatement.setDate(5, news.getModificationDate());
            preparedStatement.setLong(6, news.getNewsId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news.xml was updated by: " + news, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void delete(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news.xml was deleted by id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #addNewsAuthorList(java.lang.Long, java.util.List)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public void addNewsAuthorList(Long newsId, List<Long> authorIdList) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_AUTHOR)) {
            for (Long id : authorIdList) {
                preparedStatement.setLong(1, newsId);
                preparedStatement.setLong(2, id);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news_author.xml was created.", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #addNewsTagList(java.lang.Long, java.util.List)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public void addNewsTagList(Long newsId, List<Long> tagIdList) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_ADD_NEWS_TAG)) {
            for (Long id : tagIdList) {
                preparedStatement.setLong(1, newsId);
                preparedStatement.setLong(2, id);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news_tag.xml was created.", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #deleteNewsAuthorByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public void deleteNewsAuthorByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news_author.xml was deleted by news.xml id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #deleteNewsTagByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public void deleteNewsTagByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a news_tag.xml was deleted by news.xml id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #takeBySearchCriteria(by.epam.newsapp.entity.SearchCriteria)} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public List<Long> takeBySearchCriteria(SearchCriteria criteria) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String query;
        if (criteria != null) {
            query = makeSQLCriteria(criteria);
        } else {
            query = SQL_COMMON;
        }

        StringBuilder bd = new StringBuilder();
        bd.append(SQL_PART_ONE);
        bd.append(query);
        bd.append(SQL_PART_TWO);

        List<Long> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(bd.toString());
            while(resultSet.next()) {
                list.add(resultSet.getLong(NEWS_ID));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of news ids was taken by search criteria", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.INewsDAO #countAllNews()} )}
     * @see by.epam.newsapp.dao.INewsDAO
     */
    @Override
    public long countAllNews() throws DAOException {
        long value = 0;
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_COUNT_ALL_NEWS);
            if (resultSet.next()) {
                value = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while all news were counted", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return value;
    }

    /**
     * method builds sql_query
     * @param criteria the search criteria
     * @return sql_query string
     */
    private String makeSQLCriteria(SearchCriteria criteria) {
        List<Long> tagList = criteria.getTagIdList();
        List<Long> authorList = criteria.getAuthorIdList();
        StringBuilder bd = new StringBuilder(SQL_COMMON);
        if (!tagList.isEmpty() && !authorList.isEmpty()) {
            bd.append(SQL_TAG_AUTHOR_QUEUE);
            bd.append(makeTagsQueue(tagList));
            bd.append(" and");
            bd.append(makeAuthorsQueue(authorList));
        } else if (!tagList.isEmpty()) {
            bd.append(SQL_TAG_QUEUE);
            bd.append(makeTagsQueue(tagList));
        } else if (!authorList.isEmpty()) {
            bd.append(SQL_AUTHOR_QUEUE);
            bd.append(makeAuthorsQueue(authorList));
        }
        return bd.toString();
    }

    /**
     * method builds a query using a list of tags id
     * @param tagIdList list of tags id
     * @return query string
     */
    private String makeTagsQueue(List<Long> tagIdList) {
        StringBuilder bd = new StringBuilder();
        bd.append(" TAG_ID IN (");
        Iterator<Long> iterator = tagIdList.iterator();
        while (iterator.hasNext()) {
            bd.append(iterator.next());
            if (iterator.hasNext()) {
                bd.append(", ");
            } else {
                bd.append(")");
            }
        }
        return bd.toString();
    }

    /**
     * method builds a query using a list of authors id
     * @param authorIdList list of authors id
     * @return query string
     */
    private String makeAuthorsQueue(List<Long> authorIdList) {
        StringBuilder bd = new StringBuilder();
        bd.append(" AUTHOR_ID IN (");
        Iterator<Long> iterator = authorIdList.iterator();
        while (iterator.hasNext()) {
            bd.append(iterator.next());
            if (iterator.hasNext()) {
                bd.append(", ");
            } else {
                bd.append(")");
            }
        }
        return bd.toString();
    }


}

