package by.epam.newsapp.dao;

import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * The interface ICommentDAO
 */
public interface ICommentDAO extends IDAO<Comment, Long> {

    /**
     * method gets a list of news' comments
     * @param newsId news id
     * @return list of all comments by news id
     * @throws DAOException
     */
    List<Comment> readByNewsId(Long newsId) throws DAOException;

    /**
     * method gets a list of all comments from DB
     * @return list of all comments
     * @throws DAOException
     */
    List<Comment> readAll() throws DAOException;

    /**
     * method delete all news' comments
     * @param newsId news id
     * @throws DAOException
     */
    void deleteByNewsId(Long newsId) throws DAOException;

}
