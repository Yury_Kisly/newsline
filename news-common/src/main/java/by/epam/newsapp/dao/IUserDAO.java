package by.epam.newsapp.dao;

import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;

/**
 * The interface IUserDAO
 */
public interface IUserDAO extends IDAO<User, Long> {
    /**
     * method checks user in a db
     * @param login
     * @param password
     * @return
     * @throws DAOException
     */
    User checkUser(String login, String password) throws DAOException;
}
