package by.epam.newsapp.dao.impl.jpa;

import by.epam.newsapp.dao.INewsDAO;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.type.LongType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class NewsDAOImpl implements INewsDAO {
    private static final String JPQL_NEWS_LIST_BY_TAG_ID = "select n from News n join n.tagSet t where t.tagId =:tagId";
    private static final String JPQL_NEWS_LIST_BY_AUTHOR_ID = "select n from News n join n.authorSet a where a.authorId =:authorId";
    private static final String SQL_ADD_NEWS_TAG = "insert into \"NEWS_TAG\"(TAG_ID, NEWS_ID) values(?, ?)";
    private static final String SQL_ADD_NEWS_AUTHOR = "insert into \"NEWS_AUTHOR\"(AUTHOR_ID, NEWS_ID) values(?, ?)";
    private static final String JPQL_ALL_NEWS = "select n from News n";
    private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "delete from \"NEWS_TAG\" where NEWS_ID =?";
    private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "delete from \"NEWS_AUTHOR\" where NEWS_ID =?";

    /*Search criteria*/
    /*if no search criteria*/
    private static final String SQL_COMMON = "select distinct NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE from NEWS";
    /*if search criteria with authors list*/
    private static final String SQL_AUTHOR_QUEUE = " join NEWS_AUTHOR on (NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID) where";
    /*if search criteria with tags list*/
    private static final String SQL_TAG_QUEUE = " join NEWS_TAG on (NEWS.NEWS_ID=NEWS_TAG.NEWS_ID) where";
    /*if search criteria with tags and authors lists*/
    private static final String SQL_TAG_AUTHOR_QUEUE = " join NEWS_AUTHOR on (NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID) join NEWS_TAG on (NEWS.NEWS_ID=NEWS_TAG.NEWS_ID) where";

    private static final String SQL_PART_ONE = "select SC.NEWS_ID from ( "; //, SC.CREATION_DATE
    private static final String SQL_PART_TWO = " ) SC left join COMMENTS on SC.NEWS_ID=COMMENTS.NEWS_ID group by SC.NEWS_ID, SC.CREATION_DATE order by count(COMMENTS.COMMENT_ID) desc, SC.CREATION_DATE desc";



    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<News> readAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_ALL_NEWS);
        List<News> list = (List<News>)query.getResultList();
        return list;
    }

    @Override
    public List<News> readByAuthorId(Long authorId) throws DAOException {
        Query query = entityManager.createQuery(JPQL_NEWS_LIST_BY_AUTHOR_ID);
        query.setParameter("authorId", authorId);
        List<News> list = (List<News>)query.getResultList();
        return list;
    }

    @Override
    public List<News> readByTagId(Long tagId) throws DAOException {
        Query query = entityManager.createQuery(JPQL_NEWS_LIST_BY_TAG_ID);
        query.setParameter("tagId", tagId);
        List<News> list = (List<News>)query.getResultList();
        return list;
    }

    @Override
    public void addNewsAuthorList(Long newsId, List<Long> authorId) throws DAOException {
        Query query = entityManager.createNativeQuery(SQL_ADD_NEWS_AUTHOR);
        for(Long id : authorId) {
            query.setParameter(1, newsId);
            query.setParameter(2, id);
            query.executeUpdate();
        }
        entityManager.flush();
    }

    @Override
    public void addNewsTagList(Long newsId, List<Long> tagId) throws DAOException {
        Query query = entityManager.createNativeQuery(SQL_ADD_NEWS_TAG);
        for(Long id : tagId) {
            query.setParameter(1, newsId);
            query.setParameter(2, id);
            query.executeUpdate();
        }
        entityManager.flush();
    }

    @Override
    public void deleteNewsAuthorByNewsId(Long newsId) throws DAOException {
        Query query = entityManager.createNativeQuery(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
        query.setParameter(1, newsId);
        query.executeUpdate();
    }

    @Override
    public void deleteNewsTagByNewsId(Long newsId) throws DAOException {
        Query query = entityManager.createNativeQuery(SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
        query.setParameter(1, newsId);
        query.executeUpdate();
    }

    @Override
    public List<Long> takeBySearchCriteria(SearchCriteria criteria) throws DAOException {
        String querySQL;
        if (criteria != null) {
            querySQL = makeSQLCriteria(criteria);
        } else {
            querySQL = SQL_COMMON;
        }

        StringBuilder bd = new StringBuilder();
        bd.append(SQL_PART_ONE);
        bd.append(querySQL);
        bd.append(SQL_PART_TWO);

        Query query = entityManager.createNativeQuery(bd.toString());
        List<BigDecimal> list = (List<BigDecimal>)query.getResultList();

        List<Long> newList = new ArrayList<>();
        for( BigDecimal elem : list) {
            newList.add(elem.longValue());
        }

        return newList;
    }

    @Override
    public long countAllNews() throws DAOException {
        return 0;
    }

    @Override
    public Long create(News news) throws DAOException {
        entityManager.persist(news);
        entityManager.flush();
        return news.getNewsId();
    }

    @Override
    public News readById(Long newsId) throws DAOException {
        News news = entityManager.find(News.class, newsId);
        return news;
    }

    @Override
    public void update(News news) throws DAOException {
        entityManager.merge(news);
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        News News = entityManager.getReference(News.class, newsId);
        entityManager.remove(News);
    }

    private String makeSQLCriteria(SearchCriteria criteria) {
        List<Long> tagList = criteria.getTagIdList();
        List<Long> authorList = criteria.getAuthorIdList();
        StringBuilder bd = new StringBuilder(SQL_COMMON);
        if (!tagList.isEmpty() && !authorList.isEmpty()) {
            bd.append(SQL_TAG_AUTHOR_QUEUE);
            bd.append(makeTagsQueue(tagList));
            bd.append(" and");
            bd.append(makeAuthorsQueue(authorList));
        } else if (!tagList.isEmpty()) {
            bd.append(SQL_TAG_QUEUE);
            bd.append(makeTagsQueue(tagList));
        } else if (!authorList.isEmpty()) {
            bd.append(SQL_AUTHOR_QUEUE);
            bd.append(makeAuthorsQueue(authorList));
        }
        return bd.toString();
    }

    /**
     * method builds a query using a list of tags id
     * @param tagIdList list of tags id
     * @return query string
     */
    private String makeTagsQueue(List<Long> tagIdList) {
        StringBuilder bd = new StringBuilder();
        bd.append(" TAG_ID IN (");
        Iterator<Long> iterator = tagIdList.iterator();
        while (iterator.hasNext()) {
            bd.append(iterator.next());
            if (iterator.hasNext()) {
                bd.append(", ");
            } else {
                bd.append(")");
            }
        }
        return bd.toString();
    }

    /**
     * method builds a query using a list of authors id
     * @param authorIdList list of authors id
     * @return query string
     */
    private String makeAuthorsQueue(List<Long> authorIdList) {
        StringBuilder bd = new StringBuilder();
        bd.append(" AUTHOR_ID IN (");
        Iterator<Long> iterator = authorIdList.iterator();
        while (iterator.hasNext()) {
            bd.append(iterator.next());
            if (iterator.hasNext()) {
                bd.append(", ");
            } else {
                bd.append(")");
            }
        }
        return bd.toString();
    }
}
