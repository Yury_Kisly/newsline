package by.epam.newsapp.dao.impl.hibernate;


import by.epam.newsapp.dao.ITagDAO;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public class TagDAOImpl  implements ITagDAO {

    private static final String HQL_TAG_LIST_BY_NEWS_ID = "select t from Tag t join t.newsSet n where n.newsId =:newsId";

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Tag> readAll() throws DAOException {
        List<Tag> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            list = (List<Tag>)currentSession.createCriteria(Tag.class).list();
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of tags was taken", e);
        }

        return list;
    }

    @Override
    public List<Tag> readByNewsId(Long newsId) throws DAOException {
        List<Tag> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createQuery(HQL_TAG_LIST_BY_NEWS_ID);
            query.setParameter("newsId", newsId);
            list = (List<Tag>)query.list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of tags was taken by news id : " + newsId, e);
        }
        return list;
    }

    @Override
    public void deleteNewsTagByTagId(Long tagId) throws DAOException {
        //каскадное удаление hibernate
    }

    @Override
    public Long create(Tag tag) throws DAOException {
        Serializable tagId;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            tagId = currentSession.save(tag);
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a tag was created:", e);
        }
        return (Long)tagId;
    }

    @Override
    public Tag readById(Long tagId) throws DAOException {
        Tag tag;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            tag = (Tag)currentSession.get(Tag.class, tagId);
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a tag was taken by id: " + tagId, e);
        }
        return tag;
    }

    @Override
    public void update(Tag tag) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            currentSession.update(tag);
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a tag was updated by: " + tag, e);
        }
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Tag tag = (Tag)currentSession.createCriteria(Tag.class).add(Restrictions.idEq(tagId)).uniqueResult();
            if (tag != null) {
                currentSession.delete(tag);
//                currentSession.flush();
            }
        } catch (HibernateException e) {
            throw new DAOException("SQLException while tag was deleted by id: " + tagId, e );
        }
    }
}
