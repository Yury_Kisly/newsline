package by.epam.newsapp.dao.impl.jpa;

import by.epam.newsapp.dao.IUserDAO;
import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class UserDAOImpl implements IUserDAO {
    private static final String JPQL_CHECK_USER = "select u from User u where u.login =:login and u.password =:password";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User checkUser(String login, String password) throws DAOException {
        Query query = entityManager.createQuery(JPQL_CHECK_USER);
        query.setParameter("login", login);
        query.setParameter("password", password);
        User user = (User)query.getSingleResult();
        return user;
    }

    @Override
    public Long create(User user) throws DAOException {
        entityManager.persist(user);
        entityManager.flush();
        return user.getUserId();
    }

    @Override
    public User readById(Long userId) throws DAOException {
        User user = entityManager.getReference(User.class, userId);
        return user;
    }

    @Override
    public void update(User user) throws DAOException {
        entityManager.merge(user);
    }

    @Override
    public void delete(Long userId) throws DAOException {
        User user = entityManager.getReference(User.class, userId);
        entityManager.remove(user);
    }
}
