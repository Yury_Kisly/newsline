package by.epam.newsapp.dao.impl.jpa;


import by.epam.newsapp.dao.ITagDAO;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.List;


public class TagDAOImpl implements ITagDAO {

    private static final String JPQL_TAG_LIST_BY_NEWS_ID = "select t from Tag t join t.newsSet n where n.newsId =:newsId";
    private static final String JPQL_ALL_TAGS = "select t from Tag t";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Tag> readAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_ALL_TAGS);
        return (List<Tag>)query.getResultList();
    }

    @Override
    public List<Tag> readByNewsId(Long newsId) throws DAOException {
        Query query = entityManager.createQuery(JPQL_TAG_LIST_BY_NEWS_ID);
        query.setParameter("newsId", newsId);
        return (List<Tag>)query.getResultList();
    }

    @Override
    public void deleteNewsTagByTagId(Long tagId) throws DAOException {
    }

    @Override
    public Long create(Tag tag) throws DAOException {
        entityManager.persist(tag);
        entityManager.flush();
        return tag.getTagId();
    }

    @Override
    public Tag readById(Long tagId) throws DAOException {
        Tag tag = entityManager.find(Tag.class, tagId);
        return tag;
    }

    @Override
    public void update(Tag tag) throws DAOException {
        entityManager.merge(tag);
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        Tag tag = entityManager.getReference(Tag.class, tagId);
        entityManager.remove(tag);
    }
}
