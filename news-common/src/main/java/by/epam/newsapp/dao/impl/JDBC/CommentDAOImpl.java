package by.epam.newsapp.dao.impl.JDBC;


import by.epam.newsapp.dao.ICommentDAO;
import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//@Transactional(propagation = Propagation.MANDATORY)
public class CommentDAOImpl implements ICommentDAO{
    private static final String SQL_CREATE_COMMENT = "insert into COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) values(COMMENTS_SEQ.NEXTVAL, ?, ?, ?)";
    private static final String SQL_READ_COMMENT_BY_ID = "select COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE from COMMENTS where COMMENT_ID=?";
    private static final String SQL_DEAD_ALL_COMMENTS = "select COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE from COMMENTS";
    private static final String SQL_READ_COMMENTS_BY_NEWS_ID = "select COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE from COMMENTS where NEWS_ID=?";
    private static final String SQL_UPDATE_COMMENT = "update COMMENTS set NEWS_ID=?, COMMENT_TEXT=?, CREATION_DATE = ? where COMMENT_ID=?";
    private static final String SQL_DELETE_COMMENT = "delete from COMMENTS where COMMENT_ID=?";
    private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID = "delete from COMMENTS where NEWS_ID=?";

    private static final String COMMENT_ID = "COMMENT_ID";
    private static final String NEWS_ID = "NEWS_ID";
    private static final String COMMENT_TEXT = "COMMENT_TEXT";
    private static final String CREATION_DATE = "CREATION_DATE";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #create(by.epam.newsapp.entity.Comment)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Long create(Comment comment) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String[] column = {COMMENT_ID}; //for Oracle to prevent empty ResultSet from getGeneratedKeys()
        Long commentId = null;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_COMMENT, column)) {
            preparedStatement.setLong(1, comment.getNewsId());
            preparedStatement.setString(2, comment.getCommentText());
            preparedStatement.setTimestamp(3, comment.getCreationDate());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                commentId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a comment was created: " + comment, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return commentId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Comment readById(Long commentId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Comment comment = null;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_COMMENT_BY_ID)) {
            preparedStatement.setLong(1, commentId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                comment = new Comment();
                comment.setCommentId(resultSet.getLong(COMMENT_ID));
                comment.setNewsId(resultSet.getLong(NEWS_ID));
                comment.setCommentText(resultSet.getString(COMMENT_TEXT));
                comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a comment was taken by id: " + commentId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return comment;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.ICommentDAO #readByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.ICommentDAO
     */
    @Override
    public List<Comment> readByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Comment> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_COMMENTS_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                Comment comment = new Comment();
                comment.setCommentId(resultSet.getLong(COMMENT_ID));
                comment.setNewsId(resultSet.getLong(NEWS_ID));
                comment.setCommentText(resultSet.getString(COMMENT_TEXT));
                comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of tags was taken by news.xml id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.ICommentDAO #readAll()} )}
     * @see by.epam.newsapp.dao.ICommentDAO
     */
    @Override
    public List<Comment> readAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Comment> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_DEAD_ALL_COMMENTS);
            while(resultSet.next()) {
                Comment comment = new Comment();
                comment.setCommentId(resultSet.getLong(COMMENT_ID));
                comment.setNewsId(resultSet.getLong(NEWS_ID));
                comment.setCommentText(resultSet.getString(COMMENT_TEXT));
                comment.setCreationDate(resultSet.getTimestamp(CREATION_DATE));
                list.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of tags was taken.", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #update(by.epam.newsapp.entity.Comment)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void update(Comment comment) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COMMENT)) {
            preparedStatement.setLong(1, comment.getNewsId());
            preparedStatement.setString(2, comment.getCommentText());
            preparedStatement.setTimestamp(3, comment.getCreationDate());
            preparedStatement.setLong(4, comment.getCommentId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a comment was updated by: " + comment, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void delete(Long commentId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENT)) {
            preparedStatement.setLong(1, commentId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a comment was deleted by id: " + commentId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.ICommentDAO #deleteByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.ICommentDAO
     */
    @Override
    public void deleteByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_COMMENTS_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a comment was deleted by news.xml id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }
}
