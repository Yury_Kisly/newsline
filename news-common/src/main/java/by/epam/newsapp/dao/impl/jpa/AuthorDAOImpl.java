package by.epam.newsapp.dao.impl.jpa;

import by.epam.newsapp.dao.IAuthorDAO;
import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.DAOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.List;

public class AuthorDAOImpl implements IAuthorDAO {
    private static final String JPQL_AUTHOR_LIST_BY_NEWS_ID = "select a from Author a join a.newsSet n where n.newsId =:newsId";;
    private static final String JPQL_ALL_AUTHORS = "select a from Author a";

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Author> readAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_ALL_AUTHORS);
        return (List<Author>)query.getResultList();
    }

    @Override
    public List<Author> readByNewsId(Long newsId) throws DAOException {
        Query query = entityManager.createQuery(JPQL_AUTHOR_LIST_BY_NEWS_ID);
        query.setParameter("newsId", newsId);
        return (List<Author>)query.getResultList();
    }

    @Override
    public void authorExpire(Long authorId) throws DAOException {
        Timestamp time = new Timestamp(System.currentTimeMillis());
        Author author = entityManager.getReference(Author.class, authorId);
        author.setExpired(time);
        entityManager.merge(author);
    }

    @Override
    public Long create(Author author) throws DAOException {
        entityManager.persist(author);
        entityManager.flush();
        return author.getAuthorId();
    }

    @Override
    public Author readById(Long authorId) throws DAOException {
        Author author = entityManager.getReference(Author.class, authorId);
        return author;
    }

    @Override
    public void update(Author author) throws DAOException {
        entityManager.merge(author);
    }

    @Override
    public void delete(Long authorId) throws DAOException {
        Author author = entityManager.getReference(Author.class, authorId);
        entityManager.remove(author);
    }
}
