package by.epam.newsapp.dao.impl.JDBC;


import by.epam.newsapp.dao.IUserDAO;
import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//@Transactional(propagation = Propagation.MANDATORY)
public class UserDAOImpl implements IUserDAO{
    private static String SQL_CREATE_USER = "insert into USERS (USER_ID, USER_NAME, LOGIN, PASSWORD, ROLE_ID) VALUES (USERS_SEQ.NEXTVAL, ?, ?, ?, ?)";
    private static String SQL_READ_USER_BY_ID = "select USER_ID, USER_NAME, LOGIN, PASSWORD, ROLE_ID from USERS where USER_ID=?";
    private static String SQL_CHECK_USER = "SELECT USERS.USER_ID, USERS.USER_NAME, USERS.LOGIN, USERS.PASSWORD, USERS.ROLE_ID, ROLES.ROLE_NAME " +
            "FROM USERS JOIN ROLES ON USERS.ROLE_ID=ROLES.ROLE_ID WHERE USERS.LOGIN=? AND USERS.PASSWORD=?";
    private static String SQL_READ_USERS = "SELECT user_id, user_name, login, password, role_id FROM user";
    private static String SQL_UPDATE_USER = "update USERS set USER_NAME=?, LOGIN=?, PASSWORD=?, ROLE_ID=? WHERE USER_ID=?";
    private static String SQL_DELETE = "delete from USERS where USER_ID=?";

    private static final String USER_ID = "USER_ID";
    private static final String USER_NAME = "USER_NAME";
    private static final String LOGIN = "LOGIN";
    private static final String PASSWORD = "PASSWORD";
    private static final String ROLE_ID = "ROLE_ID";
    private static final String ROLE_NAME = "ROLE_NAME";

    @Autowired
    private DataSource dataSource;


    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #create(by.epam.newsapp.entity.User)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Long create(User user) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String[] column = {USER_ID};
        Long userId = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_USER, column)) {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setLong(4, user.getRole().getRoleId());
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                userId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a user was created: " + user, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return userId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public User readById(Long userId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        User user = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_USER_BY_ID)) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.setUserId(resultSet.getLong(USER_ID));
                user.setUserName(resultSet.getString(USER_NAME));
                user.setLogin(resultSet.getString(LOGIN));
                user.setPassword(resultSet.getString(PASSWORD));
                user.getRole().setRoleId(resultSet.getLong(ROLE_ID));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a user was taken by id: " + userId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return user;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #update(by.epam.newsapp.entity.Tag)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void update(User user) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_USER );) {
            preparedStatement.setString(1, user.getUserName());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setLong(4, user.getRole().getRoleId());
            preparedStatement.setLong(5, user.getUserId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a user was updated by user: " + user, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void delete(Long userId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)){
            preparedStatement.setLong(1, userId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while user was deleted by id: " + userId, e );
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IUserDAO #checkUser(java.lang.String, java.lang.String)} )}
     * @see by.epam.newsapp.dao.IUserDAO
     */
    @Override
    public User checkUser(String login, String password) throws DAOException {
        Connection cn = DataSourceUtils.getConnection(dataSource);
        User user = null;
        try(PreparedStatement preparedStatement = cn.prepareStatement(SQL_CHECK_USER)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = new User();
                user.setUserId(resultSet.getLong(USER_ID));
                user.setUserName(resultSet.getString(USER_NAME));
                user.setLogin(resultSet.getString(LOGIN));
                user.setPassword(resultSet.getString(PASSWORD));
                user.getRole().setRoleId(resultSet.getLong(ROLE_ID));
                user.getRole().setRoleName(resultSet.getString(ROLE_NAME));
            }

        } catch (SQLException e) {
            throw new DAOException("SQLException while user was checking.", e );
        } finally {
            DataSourceUtils.releaseConnection(cn, dataSource);
        }
        return user;
    }
}
