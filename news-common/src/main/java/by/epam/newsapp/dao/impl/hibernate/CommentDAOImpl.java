package by.epam.newsapp.dao.impl.hibernate;

import by.epam.newsapp.dao.ICommentDAO;
import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public class CommentDAOImpl implements ICommentDAO {
    private static final String HQL_READ_BY_NEWS_ID = "select c from Comment c where newsId =:newsId";
    private static final String NEWS_ID = "newsId";

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Comment> readByNewsId(Long newsId) throws DAOException {
        List<Comment> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createQuery(HQL_READ_BY_NEWS_ID);
            query.setParameter(NEWS_ID, newsId);
            list = (List<Comment>)query.list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of tags was taken by news id: " + newsId, e);
        }
        return list;
    }

    @Override
    public List<Comment> readAll() throws DAOException {
        List<Comment> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            list = (List<Comment>)currentSession.createCriteria(Comment.class).list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of tags was taken.", e);
        }
        return list;
    }

    @Override
    public void deleteByNewsId(Long newsId) throws DAOException {
        //hibernate cascade="delete"
    }

    @Override
    public Long create(Comment comment) throws DAOException {
        Serializable id;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            id = currentSession.save(comment);
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a comment was created: " + comment, e);
        }
        return (Long)id;
    }

    @Override
    public Comment readById(Long commentId) throws DAOException {
        Comment comment;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            comment = (Comment)currentSession.get(Comment.class, commentId);
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a comment was taken by id: " + commentId, e);
        }
        return comment;
    }

    @Override
    public void update(Comment comment) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            currentSession.update(comment);
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a comment was updated by: " + comment, e);
        }
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Comment comment = (Comment)currentSession.createCriteria(Comment.class)
                                .add(Restrictions.idEq(commentId)).uniqueResult();
            if (comment != null) {
                currentSession.delete(comment);
                currentSession.flush();
            }
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a comment was deleted by id: " + commentId, e);
        }
    }
}
