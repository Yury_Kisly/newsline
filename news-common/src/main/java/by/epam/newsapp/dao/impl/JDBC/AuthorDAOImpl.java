package by.epam.newsapp.dao.impl.JDBC;


import by.epam.newsapp.dao.IAuthorDAO;
import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.DAOException;

import org.apache.tomcat.jdbc.pool.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The class AuthorDAOImpl
 */
//@Transactional(propagation = Propagation.MANDATORY)
public class AuthorDAOImpl implements IAuthorDAO{
    private static final String SQL_CREATE_AUTHOR = "insert into AUTHOR (AUTHOR_ID, AUTHOR_NAME) values (AUTHOR_SEQ.NEXTVAL,?)";
    private static final String SQL_READ_AUTHOR_BY_ID = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR where AUTHOR_ID=?";
    private static final String SQL_READ_ALL_AUTHORS = "select AUTHOR_ID, AUTHOR_NAME, EXPIRED from AUTHOR";
    private static final String SQL_READ_TAGS_BY_NEWS_ID = "select AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME, AUTHOR.EXPIRED from AUTHOR join NEWS_AUTHOR on AUTHOR.AUTHOR_ID=NEWS_AUTHOR.AUTHOR_ID where NEWS_AUTHOR.NEWS_ID=?";
    private static final String SQL_UPDATE_AUTHOR = "update AUTHOR set AUTHOR_NAME=?, EXPIRED=? where AUTHOR_ID=?";
    private static final String SQL_UPDATE_EXPIRED = "update AUTHOR set EXPIRED=? where AUTHOR_ID=?";
    private static final String SQL_DELETE_AUTHOR = "delete from AUTHOR where AUTHOR_ID=?";

    private static final String AUTHOR_ID = "AUTHOR_ID";
    private static final String AUTHOR_NAME = "AUTHOR_NAME";
    private static final String EXPIRED = "EXPIRED";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #create(by.epam.newsapp.entity.Author)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Long create(Author author) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String[] column = {AUTHOR_ID};  //for Oracle to prevent empty ResultSet from getGeneratedKeys()
        Long authorId = null;
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_AUTHOR,column)) {
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.executeUpdate();

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                authorId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while an author was created: " + author, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return authorId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Author readById(Long authorId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Author author = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_AUTHOR_BY_ID)) {
            preparedStatement.setLong(1, authorId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                author = new Author();
                author.setAuthorId(resultSet.getLong(AUTHOR_ID));
                author.setAuthorName(resultSet.getString(AUTHOR_NAME));
                author.setExpired(resultSet.getTimestamp(EXPIRED));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while an author was readed by id: " + authorId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return author;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IAuthorDAO #readAll()} )}
     * @see by.epam.newsapp.dao.IAuthorDAO
     */
    @Override
    public List<Author> readAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Author> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_READ_ALL_AUTHORS);
            while(resultSet.next()) {
                Author author = new Author();
                author.setAuthorId(resultSet.getLong(AUTHOR_ID));
                author.setAuthorName(resultSet.getString(AUTHOR_NAME));
                author.setExpired(resultSet.getTimestamp(EXPIRED));
                list.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of authors was taken", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }


    /**
     * Implementation of method {@link by.epam.newsapp.dao.IAuthorDAO #readByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IAuthorDAO
     */
    @Override
    public List<Author> readByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Author> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_TAGS_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                Author author = new Author();
                author.setAuthorId(resultSet.getLong(AUTHOR_ID));
                author.setAuthorName(resultSet.getString(AUTHOR_NAME));
                author.setExpired(resultSet.getTimestamp(EXPIRED));
                list.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of authors was taken by news.xml id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IAuthorDAO #authorExpire(by.epam.newsapp.entity.Author)} )}
     * @see by.epam.newsapp.dao.ITagDAO
     */
    @Override
    public void authorExpire(Long authorId) throws DAOException {       //expired - means author date < current date
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_EXPIRED)) {
            Timestamp time = new Timestamp(System.currentTimeMillis());
            preparedStatement.setTimestamp(1, time);
            preparedStatement.setLong(2, authorId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a author's expired was modify", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #update(by.epam.newsapp.entity.Author)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void update(Author author) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_AUTHOR)) {
            preparedStatement.setString(1, author.getAuthorName());
            preparedStatement.setTimestamp(2, author.getExpired());
            preparedStatement.setLong(3, author.getAuthorId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while an author was updated by: " + author, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void delete(Long authorId) throws DAOException {
        throw new UnsupportedOperationException();
    }
}
