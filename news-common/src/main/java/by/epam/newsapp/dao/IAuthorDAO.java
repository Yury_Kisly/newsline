package by.epam.newsapp.dao;

import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * The interface IAuthorDAO
 */
public interface IAuthorDAO extends IDAO<Author, Long> {

    /**
     * method gets a list of all authors from DB
     * @return list of all authors
     * @throws DAOException
     */
    List<Author> readAll() throws DAOException;

    /**
     * gets a list of news' authors
     * @param newsId news id
     * @return list of authors by news id
     * @throws DAOException
     */
    List<Author> readByNewsId(Long newsId) throws DAOException;

    /**
     * method changes author expire date for a current
     * @param authorId
     * @throws DAOException
     */
    void authorExpire(Long authorId) throws DAOException;
}
