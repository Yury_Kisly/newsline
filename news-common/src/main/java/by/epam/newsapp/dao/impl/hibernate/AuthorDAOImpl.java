package by.epam.newsapp.dao.impl.hibernate;

import by.epam.newsapp.dao.IAuthorDAO;
import by.epam.newsapp.entity.Author;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public class AuthorDAOImpl implements IAuthorDAO {
    private static final String HQL_AUTHOR_LIST_BY_NEWS_ID = "select a from Author a join a.newsSet n where n.newsId =:newsId";

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<Author> readAll() throws DAOException {
        List<Author> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            list = (List<Author>)currentSession.createCriteria(Author.class).list();
        }catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of authors was taken", e);
        }
        return list;
    }

    @Override
    public List<Author> readByNewsId(Long newsId) throws DAOException {
        List<Author> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createQuery(HQL_AUTHOR_LIST_BY_NEWS_ID);
            query.setParameter("newsId", newsId);
            list = (List<Author>)query.list();
        }catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of authors was taken", e);
        }
        return list;
    }

    @Override
    public void authorExpire(Long authorId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Author author = (Author)currentSession.get(Author.class,authorId);
            if (author != null) {
                Timestamp time = new Timestamp(System.currentTimeMillis());
                author.setExpired(time);
                currentSession.flush();
            }
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a author's expired was modify", e);
        }
    }

    @Override
    public Long create(Author author) throws DAOException {
        Serializable val;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            val = currentSession.save(author);
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while an author was created: " + author, e);
        }
        return (Long)val;
    }

    @Override
    public Author readById(Long authorId) throws DAOException {
        Author author;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            author = (Author)currentSession.get(Author.class,authorId);
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while an author was taken by id: " + authorId, e);
        }
        return author;
    }

    @Override
    public void update(Author author) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            currentSession.update(author);
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while an author was updated by: " + author, e);
        }
    }

    @Override
    public void delete(Long authorId) throws DAOException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
