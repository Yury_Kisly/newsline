package by.epam.newsapp.dao;

import by.epam.newsapp.exception.DAOException;

/**
 * The interface IDAO contains CRUD methods for DB
 * @param <E>   generic type
 * @param <T>   generic type
 */
public interface IDAO<E, T> {
    /**
     * method adds an element to database
     * @param element
     * @return element id
     * @throws DAOException
     */
    T create(E element) throws DAOException;

    /**
     * method takes current element by element id
     * @param elemId
     * @return an entity
     * @throws DAOException
     */
    E readById(T elemId) throws DAOException;

    /**
     * method updates element
     * @param element
     * @throws DAOException
     */
    void update (E element) throws DAOException;

    /**
     * method deletes current element by element id
     * @param elemId
     * @throws DAOException
     */
    void delete(T elemId) throws DAOException;
}
