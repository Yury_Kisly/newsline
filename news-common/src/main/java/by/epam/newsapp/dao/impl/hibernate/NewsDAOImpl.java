package by.epam.newsapp.dao.impl.hibernate;

import by.epam.newsapp.dao.INewsDAO;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;
import org.hibernate.*;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.LongType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import javax.persistence.OptimisticLockException;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

@Transactional(propagation = Propagation.MANDATORY)
public class NewsDAOImpl implements INewsDAO {

    private static final String HQL_GET_NEWS_BY_AUTHOR_ID = "from News n join n.authorSet a where a.authorId =:authorId";
    private static final String HQL_GET_NEWS_BY_TAG_ID = "from News n join n.tagSet t where t.tagId =:tagId";
    private static final String SQL_DELETE_NEWS_TAG_BY_NEWS_ID = "delete from \"NEWS_TAG\" where NEWS_ID =:newsId";
    private static final String SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID = "delete from \"NEWS_AUTHOR\" where NEWS_ID =:newsId";
    private static final String SQL_ADD_NEWS_TAG = "insert into \"NEWS_TAG\"(TAG_ID, NEWS_ID) values(:tagId, :newsId)";
    private static final String SQL_ADD_NEWS_AUTHOR = "insert into \"NEWS_AUTHOR\"(AUTHOR_ID, NEWS_ID) values(:authorId, :newsId)";

    /*Search criteria*/
    /*if no search criteria*/
    private static final String SQL_COMMON = "select distinct NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE from NEWS";
    /*if search criteria with authors list*/
    private static final String SQL_AUTHOR_QUEUE = " join NEWS_AUTHOR on (NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID) where";
    /*if search criteria with tags list*/
    private static final String SQL_TAG_QUEUE = " join NEWS_TAG on (NEWS.NEWS_ID=NEWS_TAG.NEWS_ID) where";
    /*if search criteria with tags and authors lists*/
    private static final String SQL_TAG_AUTHOR_QUEUE = " join NEWS_AUTHOR on (NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID) join NEWS_TAG on (NEWS.NEWS_ID=NEWS_TAG.NEWS_ID) where";

    private static final String SQL_PART_ONE = "select SC.NEWS_ID from ( ";
    private static final String SQL_PART_TWO = " ) SC left join COMMENTS on SC.NEWS_ID=COMMENTS.NEWS_ID group by SC.NEWS_ID, SC.CREATION_DATE order by count(COMMENTS.COMMENT_ID) desc, SC.CREATION_DATE desc";

    private static final String AUTHOR_ID = "authorId";
    private static final String TAG_ID = "tagId";
    private static final String NEWS_ID = "newsId";

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<News> readAll() throws DAOException {
        List<News> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            list = (List<News>)currentSession.createCriteria(News.class).list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of news was taken", e);
        }
        return list;
    }

    @Override
    public List<News> readByAuthorId(Long authorId) throws DAOException {
        List<News> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createQuery(HQL_GET_NEWS_BY_AUTHOR_ID);
            query.setParameter(AUTHOR_ID, authorId);
            list = (List<News>)query.list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of news was taken by author id: " + authorId, e);
        }
        return list;
    }

    @Override
    public List<News> readByTagId(Long tagId) throws DAOException {
        List<News> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createQuery(HQL_GET_NEWS_BY_TAG_ID);
            query.setParameter(TAG_ID, tagId);
            list = (List<News>)query.list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of news was taken by tag id: " + tagId, e);
        }
        return list;
    }

    @Override
    public void addNewsAuthorList(Long newsId, List<Long> authorId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createSQLQuery(SQL_ADD_NEWS_AUTHOR);
            for(Long id : authorId) {
                query.setParameter(NEWS_ID, newsId);
                query.setParameter(AUTHOR_ID, id);
                query.executeUpdate();
            }
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news_author was added.", e);
        }
    }

    @Override
    public void addNewsTagList(Long newsId, List<Long> tagId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createSQLQuery(SQL_ADD_NEWS_TAG);
            for(Long id : tagId) {
                query.setParameter(NEWS_ID, newsId);
                query.setParameter(TAG_ID, id);
                query.executeUpdate();
            }
            currentSession.flush();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news_tag was added.", e);
        }
    }

    @Override
    public void deleteNewsAuthorByNewsId(Long newsId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createSQLQuery(SQL_DELETE_NEWS_AUTHOR_BY_NEWS_ID);
            query.setParameter(NEWS_ID, newsId).executeUpdate();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news_author was deleted by news id: " + newsId, e);
        }
    }

    @Override
    public void deleteNewsTagByNewsId(Long newsId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createSQLQuery(SQL_DELETE_NEWS_TAG_BY_NEWS_ID);
            query.setParameter(NEWS_ID, newsId).executeUpdate();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news_tag was deleted by news id: " + newsId, e);
        }
    }

    @Override
    public List<Long> takeBySearchCriteria(SearchCriteria criteria) throws DAOException {
        String queryStr;
        if (criteria != null) {
            queryStr = makeSQLCriteria(criteria);
        } else {
            queryStr = SQL_COMMON;
        }

        StringBuilder bd = new StringBuilder();
        bd.append(SQL_PART_ONE);
        bd.append(queryStr);
        bd.append(SQL_PART_TWO);
        List<Long> list;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            Query query = currentSession.createSQLQuery(bd.toString()).addScalar("NEWS_ID", LongType.INSTANCE);
            list = (List<Long>)query.list();
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a list of news ids was taken by search criteria", e);
        }
        return list;
    }

    @Override
    public long countAllNews() throws DAOException {
        return 0;
    }

    @Override
    public Long create(News news) throws DAOException {
        Serializable id;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            id = currentSession.save(news);
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news was created: " + news, e);
        }
        return (Long)id;
    }

    @Override
    public News readById(Long newsId) throws DAOException {
        News news;
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            news = (News)currentSession.get(News.class, newsId);
        } catch (HibernateException e) {
            System.out.println("HibDAOException=" + e);
            throw new DAOException("Hibernate exception while a news.xml was taken by id: " + newsId, e);
        }
        return news;
    }

    @Override
    public void update(News news) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            currentSession.update(news);
            currentSession.flush();
        } catch (StaleObjectStateException | OptimisticLockException exp) {
            throw exp;
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news was updated by: " + news, e);
        }
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        try {
            Session currentSession = sessionFactory.getCurrentSession();
            News news = (News)currentSession.createCriteria(News.class).add(Restrictions.idEq(newsId)).uniqueResult();
            if (news != null) {
                currentSession.delete(news);
//                currentSession.flush();
            }
        } catch (HibernateException e) {
            throw new DAOException("Hibernate exception while a news.xml was deleted by id: " + newsId, e);
        }
    }

    private String makeSQLCriteria(SearchCriteria criteria) {
        List<Long> tagList = criteria.getTagIdList();
        List<Long> authorList = criteria.getAuthorIdList();
        StringBuilder bd = new StringBuilder(SQL_COMMON);
        if (!tagList.isEmpty() && !authorList.isEmpty()) {
            bd.append(SQL_TAG_AUTHOR_QUEUE);
            bd.append(makeTagsQueue(tagList));
            bd.append(" and");
            bd.append(makeAuthorsQueue(authorList));
        } else if (!tagList.isEmpty()) {
            bd.append(SQL_TAG_QUEUE);
            bd.append(makeTagsQueue(tagList));
        } else if (!authorList.isEmpty()) {
            bd.append(SQL_AUTHOR_QUEUE);
            bd.append(makeAuthorsQueue(authorList));
        }
        return bd.toString();
    }

    /**
     * method builds a query using a list of tags id
     * @param tagIdList list of tags id
     * @return query string
     */
    private String makeTagsQueue(List<Long> tagIdList) {
        StringBuilder bd = new StringBuilder();
        bd.append(" TAG_ID IN (");
        Iterator<Long> iterator = tagIdList.iterator();
        while (iterator.hasNext()) {
            bd.append(iterator.next());
            if (iterator.hasNext()) {
                bd.append(", ");
            } else {
                bd.append(")");
            }
        }
        return bd.toString();
    }

    /**
     * method builds a query using a list of authors id
     * @param authorIdList list of authors id
     * @return query string
     */
    private String makeAuthorsQueue(List<Long> authorIdList) {
        StringBuilder bd = new StringBuilder();
        bd.append(" AUTHOR_ID IN (");
        Iterator<Long> iterator = authorIdList.iterator();
        while (iterator.hasNext()) {
            bd.append(iterator.next());
            if (iterator.hasNext()) {
                bd.append(", ");
            } else {
                bd.append(")");
            }
        }
        return bd.toString();
    }
}
