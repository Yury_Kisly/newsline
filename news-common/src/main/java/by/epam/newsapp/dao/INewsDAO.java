package by.epam.newsapp.dao;


import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * The interface INewsDAO
 */
public interface INewsDAO extends IDAO<News, Long> {
    /**
     * method gets a list of all news from DB
     * @return list of all news
     * @throws DAOException
     */
    List<News> readAll() throws DAOException;

    /**
     * method gets a list of news where author is mentioned
     * @param authorId author id
     * @return list of all news by author id
     * @throws DAOException
     */
    List<News> readByAuthorId(Long authorId) throws DAOException;

    /**
     * method gets a list of news where tag is mentioned
     * @param tagId tag id
     * @return list of all news by tag id
     * @throws DAOException
     */
    List<News> readByTagId(Long tagId) throws DAOException;

    /**
     * method add authors for the news
     * @param newsId news id
     * @param authorId author id
     * @throws DAOException
     */
    void addNewsAuthorList(Long newsId, List<Long> authorId) throws DAOException;

    /**
     * method add tags for the news
     * @param newsId news id
     * @param tagId tag id
     * @throws DAOException
     */
    void addNewsTagList(Long newsId, List<Long> tagId) throws DAOException;

    /**
     * method deletes all news' authors by news id
     * @param newsId news id
     * @throws DAOException
     */
    void deleteNewsAuthorByNewsId(Long newsId) throws DAOException;

    /**
     * method deletes all news' tags by news id
     * @param newsId news id
     * @throws DAOException
     */
    void deleteNewsTagByNewsId(Long newsId) throws DAOException;

    /**
     * method gets a list of news matched the search criteria
     * @param criteria an entity contains a list of tags and authors
     * @return a list of news
     * @throws DAOException
     */
    List<Long> takeBySearchCriteria(SearchCriteria criteria) throws DAOException;

    /**
     * method counts all news in DB
     * @return the number of news
     * @throws DAOException
     */
    long countAllNews() throws DAOException;

}
