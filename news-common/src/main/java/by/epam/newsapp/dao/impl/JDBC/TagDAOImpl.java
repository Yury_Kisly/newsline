package by.epam.newsapp.dao.impl.JDBC;

import by.epam.newsapp.dao.ITagDAO;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * The class TagDAOImpl
 */
//@Transactional(propagation = Propagation.MANDATORY)
public class TagDAOImpl implements ITagDAO {
    private static final String SQL_CREATE_TAG = "insert into TAG (TAG_ID, TAG_NAME) values (TAG_SEQ.NEXTVAL, ?)";
    private static final String SQL_READ_TAG_BY_ID = "select TAG_ID, TAG_NAME from TAG where TAG_ID=?";
    private static final String SQL_READ_ALL_TAGS = "select TAG_ID, TAG_NAME from TAG";
    private static final String SQL_READ_TAGS_BY_NEWS_ID = "select TAG.TAG_ID, TAG.TAG_NAME from TAG join NEWS_TAG on TAG.TAG_ID=NEWS_TAG.TAG_ID where NEWS_TAG.NEWS_ID=?";
    private static final String SQL_UPDATE_TAG = "update TAG set TAG_NAME=? WHERE TAG_ID=?";
    private static final String SQL_DELETE_TAG = "delete from TAG where TAG_ID=?";
    private static final String SQL_DELETE_NEWS_TAG_BY_TAG_ID = "delete from NEWS_TAG where TAG_ID=?";

    private static final String TAG_ID = "TAG_ID";
    private static final String TAG_NAME = "TAG_NAME";

    @Autowired
    private DataSource dataSource;

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #create(by.epam.newsapp.entity.Tag)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Long create(Tag tag) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        String[] column = {TAG_ID}; //for Oracle to prevent empty ResultSet from getGeneratedKeys()
        Long tagId = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE_TAG, column)) {
            preparedStatement.setString(1, tag.getName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                tagId = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a tag was created: " + tag, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tagId;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #readById(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public Tag readById(Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        Tag tag = null;
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_TAG_BY_ID)) {
            preparedStatement.setLong(1, tagId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                tag = new Tag();
                tag.setTagId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a tag was taken by id: " + tagId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return tag;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.ITagDAO #readAll()} )}
     * @see by.epam.newsapp.dao.ITagDAO
     */
    @Override
    public List<Tag> readAll() throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Tag> list = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SQL_READ_ALL_TAGS);
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setTagId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
                list.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of tags was taken", e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.ITagDAO #readByNewsId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.ITagDAO
     */
    @Override
    public List<Tag> readByNewsId(Long newsId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        List<Tag> list = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_READ_TAGS_BY_NEWS_ID)) {
            preparedStatement.setLong(1, newsId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Tag tag = new Tag();
                tag.setTagId(resultSet.getLong(TAG_ID));
                tag.setName(resultSet.getString(TAG_NAME));
                list.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException("SQLException while a list of tags was taken by news.xml id: " + newsId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return list;
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #update(by.epam.newsapp.entity.Tag)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void update(Tag tag) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TAG);) {
            preparedStatement.setString(1, tag.getName());
            preparedStatement.setLong(2, tag.getTagId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while a tag was updated by tag: " + tag, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.IDAO #delete(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.IDAO
     */
    @Override
    public void delete(Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TAG)){
            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while tag was deleted by id: " + tagId, e );
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

    /**
     * Implementation of method {@link by.epam.newsapp.dao.ITagDAO #deleteNewsTagByTagId(java.lang.Long)} )}
     * @see by.epam.newsapp.dao.ITagDAO
     */
    @Override
    public void deleteNewsTagByTagId(Long tagId) throws DAOException {
        Connection connection = DataSourceUtils.getConnection(dataSource);
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_NEWS_TAG_BY_TAG_ID)) {
            preparedStatement.setLong(1, tagId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("SQLException while the tag was deleted from tag_news by tag id: " + tagId, e);
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
    }

}
