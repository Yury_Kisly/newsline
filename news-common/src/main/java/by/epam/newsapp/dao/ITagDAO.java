package by.epam.newsapp.dao;

import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;

import java.util.List;

/**
 * The interface ITagDAO
 */
public interface ITagDAO extends IDAO<Tag, Long> {

    /**
     * method gets a list of all tags from DB
     * @return list of all tags
     * @throws DAOException
     */
    List<Tag> readAll() throws DAOException;

    /**
     * method gets a list of news' tags
     * @param newsId news id
     * @return list of tags by news id
     * @throws DAOException
     */
    List<Tag> readByNewsId(Long newsId) throws DAOException;

    /**
     * method deletes a tag from news_tag table by tag id
     * @param tagId tag id
     * @throws DAOException
     */
    void deleteNewsTagByTagId(Long tagId) throws DAOException;
}
