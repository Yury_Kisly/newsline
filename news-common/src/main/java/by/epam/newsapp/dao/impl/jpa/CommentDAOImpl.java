package by.epam.newsapp.dao.impl.jpa;

import by.epam.newsapp.dao.ICommentDAO;
import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.List;


public class CommentDAOImpl implements ICommentDAO {
    private static final String JPQL_ALL_COMMENTS = "select c from Comment c";
    private static final String JPQL_READ_BY_NEWS_ID = "select c from Comment c where c.newsId =:newsId";
    private static final String JPQL_DELETE_BY_NEWS_ID = "delete from Comment where newsId =:newsId";

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public List<Comment> readByNewsId(Long newsId) throws DAOException {
        Query query = entityManager.createQuery(JPQL_READ_BY_NEWS_ID);
        query.setParameter("newsId", newsId);
        List<Comment> list = query.getResultList();
        return list;
    }

    @Override
    public List<Comment> readAll() throws DAOException {
        Query query = entityManager.createQuery(JPQL_ALL_COMMENTS);
        List<Comment> list = (List<Comment>)query.getResultList();
        return list;
    }

    @Override
    public void deleteByNewsId(Long newsId) throws DAOException {
        Query query = entityManager.createQuery(JPQL_DELETE_BY_NEWS_ID);
        query.setParameter("newsId", newsId);
        query.executeUpdate();
    }

    @Override
    public Long create(Comment comment) throws DAOException {
        entityManager.persist(comment);
        entityManager.flush();
        return comment.getCommentId();
    }

    @Override
    public Comment readById(Long commentId) throws DAOException {
        Comment comment = entityManager.find(Comment.class, commentId);
        return comment;
    }

    @Override
    public void update(Comment comment) throws DAOException {
        entityManager.merge(comment);
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        Comment comment = entityManager.getReference(Comment.class, commentId);
        entityManager.remove(comment);
    }
}
