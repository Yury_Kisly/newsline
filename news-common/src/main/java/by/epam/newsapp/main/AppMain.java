package by.epam.newsapp.main;

import by.epam.newsapp.dao.impl.hibernate.TagDAOImpl;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;

import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICompletedTagService;
import by.epam.newsapp.service.ITagService;
import by.epam.newsapp.service.impl.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AppMain {

    public static void main(String[] args) throws DAOException, ServiceException {

        ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

//
//        NewsServiceImpl newsService = (NewsServiceImpl)context.getBean("newsService");
//        News news;
//        news = newsService.readById(7L);
//        System.out.println(news.getVersion());

//        CompletedTagServiceImpl tagService = (CompletedTagServiceImpl)context.getBean("completedTagServiceImpl");
        ITagService service = (ITagService)context.getBean("tagService");
//        tagService.deleteWholeTag(12L);
        service.delete(13L);

    }
}
