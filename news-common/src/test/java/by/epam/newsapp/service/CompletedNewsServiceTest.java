package by.epam.newsapp.service;

import by.epam.newsapp.entity.*;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.impl.CompletedNewsServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CompletedNewsServiceTest {

    @Mock
    private INewsService newsService;
    @Mock
    private ITagService tagService;
    @Mock
    private IAuthorService authorService;
    @Mock
    private ICommentService commentService;

    @InjectMocks
    private CompletedNewsServiceImpl completedNewsService;

    News news = new News();
    Tag tag = new Tag();
    Author author = new Author();
    Comment comment = new Comment();

    @Before
    public void initData() {
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(Timestamp.valueOf("2016-04-15 13:56:21"));
        news.setModificationDate(Date.valueOf("2016-04-15"));

        tag.setTagId(1L);
        tag.setName("New tag");

        author.setAuthorId(1L);
        author.setAuthorName("Zykov");
        author.setExpired(Timestamp.valueOf("2016-04-15 13:56:21"));

        comment.setCommentId(1L);
        comment.setNewsId(3L);
        comment.setCommentText("Wowowowo!");
        comment.setCreationDate(Timestamp.valueOf("2016-04-15 13:56:21"));

    }

    @Test
    public void readWholeNews() throws DAOException, ServiceException {
        /*preparing*/
        List<Tag> listTag = new ArrayList<>();
        listTag.add(tag);

        List<Author> listAuthor = new ArrayList<>();
        listAuthor.add(author);

        List<Comment> commentList = new ArrayList<>();
        commentList.add(comment);

        when(newsService.readById(anyLong())).thenReturn(news);
        when(tagService.readByNewsId(anyLong())).thenReturn(listTag);
        when(authorService.readByNewsId(anyLong())).thenReturn(listAuthor);
        when(commentService.readByNewsId(anyLong())).thenReturn(commentList);
        /*action*/
        NewsTO newsTO = completedNewsService.readWholeNews(2L);
        /*result*/
        Assert.assertNotNull(newsTO);
        Assert.assertSame(newsTO.getNews(), news);
    }


}
