package by.epam.newsapp.service;

import by.epam.newsapp.dao.impl.JDBC.TagDAOImpl;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.impl.TagServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyLong;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

    @Mock
    private TagDAOImpl tagDAO;      //this mock will be injected to the TagServiceImpl

    @InjectMocks
    private TagServiceImpl tagService;

    @Test
    public void create() throws ServiceException, DAOException {
        /*preparing*/
        Tag tag = new Tag();
        tag.setTagId(10L);
        tag.setName("Ecology");
        /*action*/
        tagService.create(tag);
        /*result*/
        verify(tagDAO).create(tag);     //check if method was taken
    }

    @Test
    public void readById() throws ServiceException, DAOException {
        /*preparing*/
        Tag tag = new Tag();
        tag.setTagId(10L);
        tag.setName("Ecology");
        when(tagDAO.readById(anyLong())).thenReturn(tag);
        /*action*/
        Tag takenTag = tagService.readById(2L);
        long id = takenTag.getTagId();
        /*result*/
        verify(tagDAO).readById(2L);
        Assert.assertEquals(10L, id);
        Assert.assertEquals("Ecology", takenTag.getName());
    }

    @Test
    public void update() throws ServiceException, DAOException {
        /*preparing*/
        Tag tag = new Tag();
        /*action*/
        tagService.update(tag);
        /*result*/
        verify(tagDAO).update(tag);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        /*action*/
        tagService.delete(2L);
        /*result*/
        verify(tagDAO).delete(2L);
    }

    @Test
    public void readAll() throws ServiceException, DAOException {
        /*preparing*/
        /*2nd way to write case*/
        when(tagDAO.readAll()).thenAnswer(new Answer<List<Tag>>() {
            @Override
            public List<Tag> answer(InvocationOnMock invoke) throws Throwable {
                List<Tag> list = new ArrayList<>();
                list.add(new Tag());
                list.add(new Tag());
                list.add(new Tag());
                return list;
            }
        });
        /*action*/
        List<Tag> takenList = tagService.readAll();
        /*result*/
        verify(tagDAO).readAll();
        Assert.assertSame(3, takenList.size());
    }

    @Test
    public void readByNewsId() throws ServiceException, DAOException {
        /*preparing*/
        List<Tag> list = new ArrayList<>();
        list.add(new Tag());
        list.add(new Tag());
        when(tagDAO.readByNewsId(anyLong())).thenReturn(list);
        /*action*/
        List<Tag> takenList = tagService.readByNewsId(3L);
        /*result*/
        verify(tagDAO).readByNewsId(3L);
        Assert.assertSame(2, takenList.size());
    }


}
