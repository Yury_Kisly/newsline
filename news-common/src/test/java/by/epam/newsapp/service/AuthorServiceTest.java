package by.epam.newsapp.service;

import by.epam.newsapp.dao.impl.JDBC.AuthorDAOImpl;
import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.impl.AuthorServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

    @Mock
    private AuthorDAOImpl authorDAO;                //this mock will be injected to the TagServiceImpl

    @InjectMocks
    private AuthorServiceImpl authorService;

    @Test
    public void create() throws ServiceException, DAOException {
        /*preparing*/
        Author author = new Author();
        when(authorDAO.create(any(Author.class))).thenReturn(2L);
        /*action*/
        long authorId = authorService.create(author);
        /*result*/
        verify(authorDAO).create(author);           //check if method was taken
        Assert.assertSame(2l, authorId);
    }

    @Test
    public void readById() throws ServiceException, DAOException {
        /*preparing*/
        /*2nd way to write case*/
        when(authorService.readById(anyLong())).thenAnswer(new Answer<Author>() {
            @Override
            public Author answer(InvocationOnMock invocationOnMock) throws Throwable {
                Author author = new Author();
                author.setAuthorId(2L);
                author.setAuthorName("Colin McFarel");
                author.setExpired(Timestamp.valueOf("2017-04-16 14:56:21"));
                return author;
            }
        });
        /*action*/
        Author author = authorService.readById(2L);
        /*result*/
        verify(authorDAO).readById(2L);
        Assert.assertNotNull(author.getAuthorId());
        Assert.assertEquals("Colin McFarel", author.getAuthorName());
    }

    @Test
    public void update() throws ServiceException, DAOException {
        /*preparing*/
        Author author = new Author();
        /*action*/
        authorService.update(author);
        /*result*/
        verify(authorDAO).update(author);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        /*action*/
//        authorService.delete(2L);
        /*result*/
//        verify(authorDAO).delete(2L);
    }

    @Test
    public void readAll() throws ServiceException, DAOException {
        /*preparing*/
        List<Author> list = new ArrayList<>();
        list.add(new Author());
        list.add(new Author());
        list.add(new Author());
        list.add(new Author());
        when(authorDAO.readAll()).thenReturn(list);
        /*action*/
        List<Author> takenList = authorService.readAll();
        /*result*/
        verify(authorDAO).readAll();
        Assert.assertSame(4, takenList.size());
    }

    @Test
    public void readByNewsId() throws ServiceException, DAOException {
        /*preparing*/
        List<Author> list = new ArrayList<>();
        list.add(new Author());
        list.add(new Author());
        list.add(new Author());
        list.add(new Author());
        when(authorDAO.readByNewsId(anyLong())).thenReturn(list);
        /*action*/
        List<Author> takenList = authorService.readByNewsId(2L);
        /*result*/
        verify(authorDAO).readByNewsId(2L);
        Assert.assertSame(4, takenList.size());
    }

    @Test
    public void authorExpire() throws ServiceException, DAOException {
        /*preparing*/
        Long authorId = 2L;
        /*action*/
        authorService.authorExpire(authorId);
        /*result*/
        verify(authorDAO).authorExpire(authorId);
    }

}
