package by.epam.newsapp.service;

import by.epam.newsapp.dao.INewsDAO;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.impl.NewsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceTest {

    @Mock
    private INewsDAO newsDAO;

    @InjectMocks
    private NewsServiceImpl newsService;

    @Test
    public void create() throws ServiceException, DAOException {
        /*preparing*/
        News news = new News();
        when(newsDAO.create(any(News.class))).thenReturn(2L);
        /*action*/
        long newsId = newsService.create(news);
        /*result*/
        verify(newsDAO).create(news);           //check if method was taken
        Assert.assertSame(2L, newsId);
    }

    @Test
    public void readById() throws ServiceException, DAOException {
        /*preparing*/
        /*2nd way to write case*/
        when(newsService.readById(anyLong())).thenAnswer(new Answer<News>() {
            @Override
            public News answer(InvocationOnMock invocationOnMock) throws Throwable {
                News news = new News();
                news.setTitle("Title");
                news.setShortText("Short text");
                news.setFullText("Full text");
                news.setCreationDate(Timestamp.valueOf("2017-04-16 14:56:21"));
                return news;
            }
        });
        /*action*/
        News news = newsService.readById(2L);
        /*result*/
        verify(newsDAO).readById(2L);
        Assert.assertNotNull(news.getFullText());
        Assert.assertEquals("Title", news.getTitle());
    }

    @Test
    public void update() throws ServiceException, DAOException {
        /*preparing*/
        News news = new News();
        /*action*/
        newsService.update(news);
        /*result*/
        verify(newsDAO).update(news);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        /*action*/
        newsService.delete(2L);
        /*result*/
        verify(newsDAO).delete(2L);
    }

    @Test
    public void readAll() throws ServiceException, DAOException {
        /*preparing*/
        List<News> list = new ArrayList<>();
        list.add(new News());
        list.add(new News());
        list.add(new News());
        list.add(new News());
        when(newsDAO.readAll()).thenReturn(list);
        /*action*/
        List<News> takenList = newsService.readAll();
        /*result*/
        verify(newsDAO).readAll();
        Assert.assertSame(4, takenList.size());
    }

    @Test
    public void readByAuthorId() throws ServiceException, DAOException {
        /*preparing*/
        List<News> list = new ArrayList<>();
        list.add(new News());
        list.add(new News());
        list.add(new News());
        list.add(new News());
        when(newsDAO.readByAuthorId(anyLong())).thenReturn(list);
        /*action*/
        List<News> takenList = newsService.readByAuthorId(2L);
        /*result*/
        verify(newsDAO).readByAuthorId(2L);
        Assert.assertSame(4, takenList.size());
    }

    @Test
    public void readByTagId() throws ServiceException, DAOException {
        /*preparing*/
        List<News> list = new ArrayList<>();
        list.add(new News());
        list.add(new News());
        list.add(new News());
        when(newsDAO.readByTagId(anyLong())).thenReturn(list);
        /*action*/
        List<News> takenList = newsService.readByTagId(2L);
        /*result*/
        verify(newsDAO).readByTagId(2L);
        Assert.assertSame(3, takenList.size());
    }

    @Test
    public void addNewsAuthorList() throws ServiceException, DAOException {
        /*preparing*/
        List<Long> authorIdList = new ArrayList<>();
        authorIdList.add(3L);
        authorIdList.add(8L);
        /*action*/
        newsService.addNewsAuthorList(4L, authorIdList);
        /*result*/
        verify(newsDAO).addNewsAuthorList(4L, authorIdList);
    }

    @Test
    public void addNewsTagList() throws ServiceException, DAOException {
        /*preparing*/
        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(3L);
        tagIdList.add(8L);
        /*action*/
        newsService.addNewsTagList(2L, tagIdList);
        /*result*/
        verify(newsDAO).addNewsTagList(2L, tagIdList);
    }

    @Test
    public void deleteNewsAuthorByNewsId() throws ServiceException, DAOException {
        /*action*/
        newsService.deleteNewsAuthorByNewsId(3L);
        /*result*/
        verify(newsDAO).deleteNewsAuthorByNewsId(3L);
    }

    @Test
    public void deleteNewsTagByNewsId() throws ServiceException, DAOException {
        /*action*/
        newsService.deleteNewsTagByNewsId(3L);
        /*result*/
        verify(newsDAO).deleteNewsTagByNewsId(3L);
    }

    @Test
    public void takeBySearchCriteria() throws ServiceException, DAOException {
        /*preparing*/
        SearchCriteria criteria = new SearchCriteria();
        List<Long> list = new ArrayList<>();
        list.add(2L);
        list.add(55L);
        list.add(3L);
        when(newsDAO.takeBySearchCriteria(any(SearchCriteria.class))).thenReturn(list);
        /*action*/
        List<Long> criteriaList = newsService.takeBySearchCriteria(criteria);
        /*result*/
        verify(newsDAO).takeBySearchCriteria(criteria);
        Assert.assertSame(3, criteriaList.size());
    }

    @Test
    public void countAllNews() throws ServiceException, DAOException {
        /*preparing*/
        when(newsDAO.countAllNews()).thenReturn(5L);
        /*action*/
        long value = newsService.countAllNews();
        /*result*/
        verify(newsDAO).countAllNews();
        Assert.assertSame(5L, value);
    }



}
