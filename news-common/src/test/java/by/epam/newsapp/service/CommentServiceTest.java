package by.epam.newsapp.service;

import by.epam.newsapp.dao.impl.JDBC.CommentDAOImpl;
import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.impl.CommentServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

    @Mock
    private CommentDAOImpl commentDAO;

    @InjectMocks
    private CommentServiceImpl commentService;

    @Test
    public void create() throws ServiceException, DAOException {
        /*preparing*/
        Comment comment = new Comment();
        when(commentDAO.create(any(Comment.class))).thenReturn(2L);
        /*action*/
        long commentId = commentService.create(comment);
        /*result*/
        verify(commentDAO).create(comment);           //check if method was taken
        Assert.assertSame(2l, commentId);
    }

    @Test
    public void readById() throws ServiceException, DAOException {
        /*preparing*/
        /*2nd way to write case*/
        when(commentService.readById(anyLong())).thenAnswer(new Answer<Comment>() {
            @Override
            public Comment answer(InvocationOnMock invocationOnMock) throws Throwable {
                Comment comment = new Comment();
                comment.setCommentId(1L);
                comment.setNewsId(3L);
                comment.setCommentText("Wowowowo!");
                comment.setCreationDate(Timestamp.valueOf("2017-04-16 14:56:21"));
                return comment;
            }
        });
        /*action*/
        Comment comment = commentService.readById(1L);
        /*result*/
        verify(commentDAO).readById(1L);
        Assert.assertNotNull(comment.getCommentId());
        Assert.assertEquals("Wowowowo!", comment.getCommentText());
    }

    @Test
    public void update() throws ServiceException, DAOException {
        /*preparing*/
        Comment comment = new Comment();
        /*action*/
        commentService.update(comment);
        /*result*/
        verify(commentDAO).update(comment);
    }

    @Test
    public void delete() throws ServiceException, DAOException {
        /*action*/
        commentService.delete(2L);
        /*result*/
        verify(commentDAO).delete(2L);
    }

    @Test
    public void readAll() throws ServiceException, DAOException {
        /*preparing*/
        List<Comment> list = new ArrayList<>();
        list.add(new Comment());
        list.add(new Comment());
        list.add(new Comment());
        list.add(new Comment());
        when(commentDAO.readAll()).thenReturn(list);
        /*action*/
        List<Comment> takenList = commentService.readAll();
        /*result*/
        verify(commentDAO).readAll();
        Assert.assertSame(4, takenList.size());
    }

    @Test
    public void readByNewsId() throws ServiceException, DAOException {
        /*preparing*/
        List<Comment> list = new ArrayList<>();
        list.add(new Comment());
        list.add(new Comment());
        list.add(new Comment());
        list.add(new Comment());
        when(commentDAO.readByNewsId(anyLong())).thenReturn(list);
        /*action*/
        List<Comment> takenList = commentService.readByNewsId(2L);
        /*result*/
        verify(commentDAO).readByNewsId(2L);
        Assert.assertSame(4, takenList.size());
    }
}
