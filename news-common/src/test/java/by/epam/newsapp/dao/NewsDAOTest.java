package by.epam.newsapp.dao;


import by.epam.newsapp.entity.News;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.DAOException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
//@ContextConfiguration(locations = {"classpath:hibernate-test.xml"})
//@ContextConfiguration(locations = {"classpath:eclipselink-test.xml"})
@Transactional      //for hibernate, works only with transactions (if it will not be mentioned -> exception)
public class NewsDAOTest extends AbstractDAOTest {

    @Autowired
    private INewsDAO newsDAO;

    @Override
    public IDataSet takeDataSetToInsert() throws FileNotFoundException, DataSetException {
        IDataSet[] dataSet = {
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tag.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_author.xml"))
        };
        return new CompositeDataSet(dataSet);
    }

    @Before
    @Override
    public void configureDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
        DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    @Test
    public void create() throws DAOException {
        News news = new News();
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(System.currentTimeMillis()));
        news.setModificationDate(new Date(System.currentTimeMillis()));
        long id = newsDAO.create(news);
        Assert.assertNotEquals(0L, id);
    }

    @Test
    public void readById() throws DAOException {
        Assert.assertNotNull(newsDAO.readById(2L));
        News news = newsDAO.readById(2L);
        Assert.assertEquals("Title 2 is there", news.getTitle());
    }

    @Test
    public void readAll() throws DAOException {
        List<News> list = newsDAO.readAll();
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 4);
    }

    @Test
    public void update() throws DAOException {
        News news = newsDAO.readById(3L);
        news.setShortText("Updated short text");
        news.setFullText("Updated full text");
        newsDAO.update(news);
        Assert.assertEquals("Updated short text", newsDAO.readById(3L).getShortText());
    }

    @Test
    public void delete() throws DAOException {
        News news = new News();
        news.setTitle("Title");
        news.setShortText("Short text");
        news.setFullText("Full text");
        news.setCreationDate(new Timestamp(System.currentTimeMillis()));
        news.setModificationDate(new Date(System.currentTimeMillis()));
        Long newsId = newsDAO.create(news);
        newsDAO.delete(newsId);
        news = newsDAO.readById(newsId);
        Assert.assertNull(news);
    }

    @Test
    public void readByAuthorId() throws DAOException {
        List<News> list = newsDAO.readByAuthorId(2L);
        Assert.assertSame(list.size(), 3);
    }

    @Test
    public void readByTagId() throws DAOException {
        List<News> list = newsDAO.readByTagId(4L);
        Assert.assertSame(list.size(), 2);
    }

    @Test
    public void addNewsAuthorList() throws DAOException {
        List<Long> authorIdList = new ArrayList<>();
        authorIdList.add(1L);
        authorIdList.add(3L);
        newsDAO.addNewsAuthorList(2L, authorIdList);
        int value = newsDAO.readByAuthorId(3L).size();
        System.out.println("VALUE=" + value);
        Assert.assertSame(value, 2);
    }

    @Test
    public void addNewsTagList() throws DAOException {
        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(4L);
        tagIdList.add(1L);
        newsDAO.addNewsTagList(4L, tagIdList);
        int value = newsDAO.readByTagId(4L).size();
        System.out.println("VALUE=" + value);
        Assert.assertSame(value, 3);
    }

    @Test
    public void takeBySearchCriteria() throws DAOException {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setAuthorId(2L);
        List<Long> list = newsDAO.takeBySearchCriteria(criteria);
        Assert.assertSame(list.size(), 3);
    }

    @Test
    public void countAllNews() throws DAOException {
        long value = newsDAO.countAllNews();
        Assert.assertSame(value, 4L);
    }

}
