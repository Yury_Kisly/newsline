package by.epam.newsapp.dao;


import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.DAOException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
//@ContextConfiguration(locations = {"classpath:hibernate-test.xml"})
//@ContextConfiguration(locations = {"classpath:eclipselink-test.xml"})
@Transactional      //for hibernate, works only with transactions (if it will not be mentioned -> exception)
public class UserDAOTest extends AbstractDAOTest {

    @Autowired
    private IUserDAO userDAO;

    @Override
    public IDataSet takeDataSetToInsert() throws FileNotFoundException, DataSetException {
        IDataSet[] dataSet = {
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/roles.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/users.xml")),

        };
        return new CompositeDataSet(dataSet);
    }

    @Before
    @Override
    public void configureDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
        DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    @Test
    public void create() throws DAOException {
        User user = new User();
        user.setUserName("Devid");
        user.setLogin("McNeel");
        user.setPassword("ddd");
        user.getRole().setRoleId(2L);
        user.getRole().setRoleName("ROLE_USER");

        System.out.println("TEST user=" + user);
        System.out.println("TEST USER ROLE=" + user.getRole());

        long userId = userDAO.create(user);
        System.out.println("USER ID=" + userId);
        Assert.assertNotEquals(0, userId);
    }

    @Test
    public void readById() throws DAOException {
        Assert.assertNotNull(userDAO.readById(3L));
        String userName = userDAO.readById(3L).getUserName();
        Assert.assertEquals("Jane", userName);
    }

    @Test
    public void update() throws DAOException {
        User user = userDAO.readById(2L);
        user.setUserName("Bob");
        userDAO.update(user);
        Assert.assertEquals("Bob", userDAO.readById(2L).getUserName());
    }

    @Test
    public void delete() throws DAOException {
        User user = new User();
        user.setUserName("User for delete");
        user.setLogin("Mc");
        user.setPassword("eee");
        user.getRole().setRoleId(2L);
        user.getRole().setRoleName("ROLE_USER");
        Long userId = userDAO.create(user);
        System.out.println("USER ID="+userId);
        userDAO.delete(userId);
        user = userDAO.readById(userId);
        Assert.assertNull(user);
    }

    @Test
    public void checkUser() throws DAOException {
        String login = "Pan";
        String password = "aaa";
        User user = userDAO.checkUser(login, password);
        System.out.println("USER=" + user);
        Assert.assertNotNull(user);
    }
}
