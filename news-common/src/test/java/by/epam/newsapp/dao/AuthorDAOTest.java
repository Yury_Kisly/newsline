package by.epam.newsapp.dao;


import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.DAOException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
//@ContextConfiguration(locations = {"classpath:hibernate-test.xml"})
//@ContextConfiguration(locations = {"classpath:eclipselink-test.xml"})
@Transactional      //for hibernate, works only with transactions (if it will not be mentioned -> exception)
public class AuthorDAOTest extends AbstractDAOTest {

    @Autowired
    private IAuthorDAO authorDAO;

    @Override
    public IDataSet takeDataSetToInsert() throws FileNotFoundException, DataSetException {
        IDataSet[] dataSet = {
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/author.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_author.xml"))
        };
        return new CompositeDataSet(dataSet);
    }

    @Before
    @Override
    public void configureDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
        DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    @Test
    public void create() throws DAOException {
        Author author = new Author();
        author.setAuthorName("Zykov");
        long id = authorDAO.create(author);
        Assert.assertNotEquals(0, id);
    }

    @Test
    public void readById() throws DAOException {
        Assert.assertNotNull(authorDAO.readById(3l));
        String authorName = authorDAO.readById(3l).getAuthorName();
        Assert.assertEquals("Andrej Amalrik", authorName);
    }

    @Test
    public void readAll() throws DAOException {
        List<Author> list = authorDAO.readAll();
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 4);
    }

    @Test
    public void readByNewsId() throws DAOException {
        List<Author> list = authorDAO.readByNewsId(3L);
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 2);
    }

    @Test
    public void update() throws DAOException {
        Author author = authorDAO.readById(2L);
        author.setAuthorName("Colin McFarel");
        author.setExpired(Timestamp.valueOf("2017-04-16 14:56:21"));
        authorDAO.update(author);
        author = authorDAO.readById(2L);
        Assert.assertEquals("Colin McFarel", author.getAuthorName());
    }

    @Test
    public void authorExpire() throws DAOException {
        authorDAO.authorExpire(2L);
        Author author = authorDAO.readById(2L);
        long auhorTime = author.getExpired().getTime();
        long currentTime = System.currentTimeMillis();
        Assert.assertNotEquals(auhorTime, currentTime);
    }

}
