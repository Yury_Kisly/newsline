package by.epam.newsapp.dao;


import org.apache.tomcat.jdbc.pool.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
public abstract class AbstractDAOTest {
    private static final String XML_CLEAR_DATA = "src/test/resources/clear.xml";

    @Autowired
    DataSource dataSource;
    Connection cn;

    public IDatabaseConnection takeConnection() throws SQLException, DatabaseUnitException {
        cn = DataSourceUtils.getConnection(dataSource);
        DatabaseMetaData databaseMetaData = cn.getMetaData();
        IDatabaseConnection connection = new DatabaseConnection(cn, databaseMetaData.getUserName().toUpperCase());
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new OracleDataTypeFactory());
        return connection;
    }

    // clear all tables in the db
    public IDataSet takeDataSetToDelete() throws FileNotFoundException, DataSetException {
        IDataSet dataSet = new FlatXmlDataSetBuilder().build(new FileInputStream(XML_CLEAR_DATA));
        return  dataSet;
    }

    public abstract IDataSet takeDataSetToInsert() throws FileNotFoundException, DataSetException;

    public abstract void configureDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException;

    @After
    public void cleanDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
        DatabaseOperation.DELETE_ALL.execute(takeConnection(), takeDataSetToDelete());
    }

    @After
    public void releaseConnection() {
        DataSourceUtils.releaseConnection(cn, dataSource);
    }
}
