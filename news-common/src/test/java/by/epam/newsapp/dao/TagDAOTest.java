package by.epam.newsapp.dao;

import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.DAOException;

import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import org.junit.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
//@ContextConfiguration(locations = {"classpath:hibernate-test.xml"})
//@ContextConfiguration(locations = {"classpath:eclipselink-test.xml"})
@Transactional      //for hibernate, works only with transactions (if it will not be mentioned -> exception)
public class TagDAOTest extends AbstractDAOTest {

    @Autowired
    private ITagDAO tagDAO;

    @Override
    public IDataSet takeDataSetToInsert() throws FileNotFoundException, DataSetException {
        IDataSet[] dataSet = {
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/tag.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news_tag.xml"))
        };
        return new CompositeDataSet(dataSet);
    }


    @Before
    @Override
    public void configureDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
        DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    @Test
    public void create() throws DAOException {
        Tag tag = new Tag();
        tag.setName("New tag");
        long tagId = tagDAO.create(tag);
        System.out.println(tagId);
        Assert.assertNotEquals(0, tagId);
    }

    @Test
    public void readById() throws DAOException {
        Assert.assertNotNull(tagDAO.readById(4l));
        String tagName = tagDAO.readById(4l).getName();
        Assert.assertEquals("Economy", tagName);
    }

    @Test
    public void readAll() throws DAOException {
        List<Tag> list = tagDAO.readAll();
        for (Tag elem: list) {
            System.out.println(elem);
        }
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 4);
    }

    @Test
    public void readByNewsId() throws DAOException {
        List<Tag> list = tagDAO.readByNewsId(3L);
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 3);
    }

    @Test
    public void update() throws DAOException {
        Tag tag = tagDAO.readById(2l);
        tag.setName("Family");
        tagDAO.update(tag);
        Assert.assertEquals("Family", tagDAO.readById(2l).getName());
    }

    @Test
    public void delete() throws DAOException {
        Tag tag = new Tag();
        tag.setName("Tag for delete");
        Long tagId = tagDAO.create(tag);
        tagDAO.delete(tagId);
        System.out.println("TAG ID=" + tagId);
        tag = tagDAO.readById(tagId);
        System.out.println("TAG=" + tag);
        Assert.assertNull(tag);
    }
    @Test
    public void deleteNewsTagByTagId() throws DAOException {
        tagDAO.deleteNewsTagByTagId(3L);
    }
}