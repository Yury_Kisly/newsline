package by.epam.newsapp.dao;

import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.exception.DAOException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
//@ContextConfiguration(locations = {"classpath:hibernate-test.xml"})
//@ContextConfiguration(locations = {"classpath:eclipselink-test.xml"})
@Transactional      //for hibernate, works only with transactions (if it will not be mentioned -> exception)
public class CommentDAOTest extends AbstractDAOTest {

    @Autowired
    private ICommentDAO commentDAO;

    @Override
    public IDataSet takeDataSetToInsert() throws FileNotFoundException, DataSetException {
        IDataSet[] dataSet = {
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/news.xml")),
                new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/comments.xml"))
        };
        return new CompositeDataSet(dataSet);
    }

    @Before
    @Override
    public void configureDataBase() throws DatabaseUnitException, SQLException, FileNotFoundException {
        DatabaseOperation.CLEAN_INSERT.execute(takeConnection(), takeDataSetToInsert());
    }

    @Test
    public void create() throws DAOException {
        Comment comment = new Comment();
        Timestamp creationDate = new Timestamp(System.currentTimeMillis());
        comment.setNewsId(3L);
        comment.setCommentText("Wowowowo!");
        comment.setCreationDate(creationDate);
        long id = commentDAO.create(comment);
        Assert.assertNotEquals(0L, id);
    }

    @Test
    public void readById() throws DAOException {
        Assert.assertNotNull(commentDAO.readById(2L));
        Comment comment = commentDAO.readById(2L);
        Assert.assertEquals("Because of..", comment.getCommentText());
    }

    @Test
    public void readAll() throws DAOException {
        List<Comment> list = commentDAO.readAll();
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 5);
    }

    @Test
    public void readByNewsId() throws DAOException {
        List<Comment> list = commentDAO.readByNewsId(2L);
        Assert.assertNotNull(list);
        Assert.assertSame(list.size(), 2);
    }

    @Test
    public void update() throws DAOException {
        Comment comment = commentDAO.readById(5L);
        comment.setNewsId(3L);
        comment.setCommentText("Wowowowo!");
        commentDAO.update(comment);
        Assert.assertEquals("Wowowowo!", commentDAO.readById(5L).getCommentText());
    }

    @Test
    public void delete() throws DAOException {
        Comment comment = new Comment();
        Timestamp creationDate = new Timestamp(System.currentTimeMillis());
        comment.setNewsId(3L);
        comment.setCommentText("Wowowowo!");
        comment.setCreationDate(creationDate);
        Long commentId = commentDAO.create(comment);
        commentDAO.delete(commentId);
        comment = commentDAO.readById(commentId);
        Assert.assertNull(comment);
    }

    @Test
    public void deleteByNewsId() throws DAOException {
        commentDAO.deleteByNewsId(2L);
        Assert.assertEquals(3L, commentDAO.readAll().size());
    }
}
