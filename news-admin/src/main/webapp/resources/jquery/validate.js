$(document).ready(function(){
    var login = $('#login');
    var pass = $('#pass');
    var login_error = $('#login-error');
    var pass_error = $('#pass-error');

    // Initializing Variables With Regular Expressions
    var login_regex = /^.{4,15}$/;
    var pass_regex = /^.{5,15}$/;

    $('#login_submit').click(function(event) {
        // To Check invalid Form Fields.
        if (validLogin() & validPass()) {
            console.log('---> validation works true');
            return true;
        } else {
            return false;
        }
    });

    function  validLogin() {
        if(!login_regex.test(login.val())) {
            login_error.addClass("visible-error").removeClass("hidden-error");
            login.addClass("color-error");
            console.log('---> validation login works bad');
            return false;
        } else {
            login_error.addClass("hidden-error").removeClass("visible-error");
            login.removeClass("color-error");
            return true;
        }
    }

    function  validPass() {
        if(!pass_regex.test(pass.val())) {
            pass_error.addClass("visible-error").removeClass("hidden-error");
            pass.addClass("color-error");
            console.log('---> validation pass works bad');
            return false;
        } else {
            pass_error.addClass("hidden-error").removeClass("visible-error");
            pass.removeClass("color-error");
            return true;
        }
    }
});