var isTagEdit = false;      //to prevent 2 tags edition at one time

function edit(tagId) {
    if (!isTagEdit) {
        isTagEdit = true;
        document.getElementById("tagName"+tagId).disabled = false;
        document.getElementById("editTag"+tagId).hidden = true;
        document.getElementById("updateTag"+tagId).hidden = false;
        document.getElementById("deleteTag"+tagId).hidden = false;
        document.getElementById("cancelTag"+tagId).hidden = false;
    }
}

function cancel(tagId) {
    document.getElementById("tagName"+tagId).disabled = true;
    document.getElementById("editTag"+tagId).hidden = false;
    document.getElementById("updateTag"+tagId).hidden = true;
    document.getElementById("deleteTag"+tagId).hidden = true;
    document.getElementById("cancelTag"+tagId).hidden = true;
    isTagEdit = false;
}

