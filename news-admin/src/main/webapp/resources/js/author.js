var isAuthorEdit = false;      //to prevent 2 authors edition at one time

function editAuthor(authorId) {
    if (!isAuthorEdit) {
        isAuthorEdit = true;
        document.getElementById("authorName"+authorId).disabled = false;
        document.getElementById("editAuthor"+authorId).hidden = true;
        document.getElementById("updateAuthor"+authorId).hidden = false;
        document.getElementById("expireAuthor"+authorId).hidden = false;
        document.getElementById("cancelAuthor"+authorId).hidden = false;
    }
}

function cancelAuthor(authorId) {
    document.getElementById("authorName"+authorId).disabled = true;
    document.getElementById("editAuthor"+authorId).hidden = false;
    document.getElementById("updateAuthor"+authorId).hidden = true;
    document.getElementById("expireAuthor"+authorId).hidden = true;
    document.getElementById("cancelAuthor"+authorId).hidden = true;
    isAuthorEdit = false;
}