<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<html>
    <head>

        <spring:url value="/resources/css/style.css" var="style"/>
        <spring:url value="/resources/jquery/jquery.min.js" var="jquery"/>
        <spring:url value="/resources/jquery/validate.js" var="validate"/>

        <link rel="stylesheet" href="${style}" type="text/css" />
        <script type="text/javascript" src="${jquery}"></script>
        <script type="text/javascript" src="${validate}"></script>
    </head>

    <body>
        <div id="header"> <tiles:insertAttribute name="header"/> </div>
        <div class="body-login"> <tiles:insertAttribute name="body"/> </div>
        <div id="footer"> <tiles:insertAttribute name="footer" /> </div>
    </body>
</html>
