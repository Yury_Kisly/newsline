<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<html>
  <head>
    <spring:url value="/resources/css/style.css" var="style"/>
    <spring:url value="/resources/css/dropdown.css" var="dropdown"/>
    <spring:url value="/resources/js/author.js" var="author"/>
    <spring:url value="/resources/js/tag.js" var="tag"/>


    <link rel="stylesheet" href="${style}" type="text/css" />
    <link rel="stylesheet" href="${dropdown}" type="text/css" />
    <script type="text/javascript" src="${author}"></script>
    <script type="text/javascript" src="${tag}"></script>
  </head>
  <body>

    <div id="header">
      <tiles:insertAttribute name="header" />
    </div>
    <div id="menu">
      <tiles:insertAttribute name="menu" />
    </div>
    <div id="body">
      <tiles:insertAttribute name="body" />
    </div>
    <div id="footer">
      <tiles:insertAttribute name="footer" />
    </div>

  </body>
</html>
