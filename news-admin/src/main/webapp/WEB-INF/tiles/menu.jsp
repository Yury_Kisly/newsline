<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<html>
  <head><title></title>

  </head>
  <body>

    <spring:url value="/news-list/1" var="url_news_list"/>
    <spring:url value="/add-news" var="url_add_news"/>
    <spring:url value="/authors" var="url_authors"/>
    <spring:url value="/tags" var="url_tags"/>

    <a href="${url_news_list}"><spring:message code="menu.news-list"/></a> <br/>
    <a href="${url_add_news}"><spring:message code="menu.add-news"/></a> <br/>
    <a href="${url_authors}"><spring:message code="menu.add-authors"/></a>  <br/>
    <a href="${url_tags}"><spring:message code="menu.add-tags"/></a> <br/>

  </body>
</html>
