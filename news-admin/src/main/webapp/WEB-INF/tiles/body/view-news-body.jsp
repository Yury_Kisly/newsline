<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <title></title>
</head>
    <body>
    <spring:url value="/news-list/view/${currentNews.news.newsId}/comment" var="url_post"/>
    <spring:url value="/news-list/view/prev" var="url_prev"/>
    <spring:url value="/news-list/view/next" var="url_next"/>
    <%--set pattern for date displaying--%>
    <c:set var="datePatternView" value="MM/dd/yyyy"/>


    <%--title--%>
    <div class="title">
        <c:out value="${currentNews.news.title}"/>
    </div>
    <%--authors--%>
    <em>
        by:
        <c:forEach items="${currentNews.authorList}" var="author">
            ${author.authorName},
        </c:forEach>
        <fmt:formatDate value="${currentNews.news.creationDate}" pattern="${datePatternView}" />
    </em>
    <br>
    <%--full text--%>
    <div class="newsLine"> <c:out value="${currentNews.news.fullText}"/> </div>
    <%--comments--%>
    <c:forEach items="${currentNews.commentList}" var="comment">
        <div class="comment">
            <spring:url value="/news-list/view/${currentNews.news.newsId}/comment/${comment.commentId}" var="url_delete"/>
            <div >
                    <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
                <form:form action="${url_delete}" modelAttribute="comment" method="DELETE">
                    <form:hidden path="commentId" value="${comment.commentId}" />
                    <form:hidden path="newsId" value="${comment.newsId}" />
                    <input type="submit" value="x"><%--del comment--%>
                </form:form>
            </div>

            <div ><fmt:formatDate value="${comment.creationDate}" pattern="${datePatternView}" /> </div>
            <div><c:out value="${comment.commentText}"/></div>

        </div>
    </c:forEach>

    <%--add comment--%>
    <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
    <form:form action="${url_post}" modelAttribute="comment" method="POST" >
        <form:input path="newsId" value="${currentNews.news.newsId}" type="text" style="display:none" />
        <form:textarea path="commentText" rows="3" cols="105" placeholder="comment" />
        <form:errors path="commentText" cssClass="error" style="color:red;" /> <br/>
        <input type="submit" value="<spring:message code="comment.btn.post"/>" />
    </form:form>

    <%--PREV NEXT--%>
    <div class="prev">
        <form action="${url_prev}" method="post" >
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="newsId" value="${currentNews.news.newsId}">
            <input type="submit" value="<spring:message code="news.btn.prev"/>">
        </form>
    </div>
    <div class="next">
        <form action="${url_next}" method="post" >
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="hidden" name="newsId" value="${currentNews.news.newsId}">
            <input type="submit" value="<spring:message code="news.btn.next"/>">
        </form>
    </div>

    </body>
</html>
