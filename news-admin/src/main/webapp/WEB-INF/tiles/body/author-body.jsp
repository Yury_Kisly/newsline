<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
    </head>
    <body>

        <spring:url value="/authors" var="url_add"/>

        <c:forEach var="author" items="${authorList}">

            <spring:url value="/authors/${author.authorId}" var="url_update"/>
            <spring:url value="/authors/${author.authorId}/expire" var="url_expire"/>

            <c:if test="${empty author.expired}">
                <div class="authorLine">
                    <div class="author-author"> <spring:message code="author.author"/> </div>
                        <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
                    <form:form action="${url_update}" commandName="author" method="PUT" cssClass="btn-author-edit">
                        <form:input path="authorId" value="${author.authorId}" type="text" style="display:none" />
                        <form:input path="authorName" value="${author.authorName}" cssClass="author-text-line" type="text"
                                    id="authorName${author.authorId}" disabled="true" />
                        <!-- edit author -->
                        <div class="btn-author-edit">
                            <input type="button" id="editAuthor${author.authorId}"
                                   value="<spring:message code="author.btn.edit"/>"
                                   onclick="editAuthor(${author.authorId})" />
                        </div>
                        <!-- update  -->
                        <div class="btn-author-edit" >
                            <input type="submit" id="updateAuthor${author.authorId}" hidden="true"
                                   value="<spring:message code="author.btn.upd"/>"/>
                        </div>
                    </form:form>

                    <!-- expire -->
                    <div class="btn-author-edit">
                        <form:form action="${url_expire}" method="PUT">
                            <input type="submit" id="expireAuthor${author.authorId}" hidden="true"
                                   value="<spring:message code="author.btn.exp"/>"/>
                        </form:form>
                    </div>

                    <!-- cancel author -->
                    <div class="btn-author-edit">
                        <input type="button" id="cancelAuthor${author.authorId}"
                               onclick="cancelAuthor(${author.authorId})" hidden="true"
                               value="<spring:message code="author.btn.cancel"/>"/>
                    </div>
                </div>
            </c:if>
        </c:forEach>

        <br/>

        <div class="authorLine">
            <div class="author-author"><spring:message code="author.add-author" /></div>
            <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
            <form:form action="${url_add}" commandName="author" method="POST" cssClass="btn-author-edit">
                <form:input path="authorName" type="text" cssClass="author-text-line" />
                <div class="btn-author-edit">
                    <input type="submit" value="<spring:message code="author.btn.save" />" />
                </div>
                <form:errors path="authorName" cssClass="error" style="color:red;"/>
            </form:form>
        </div>
    </body>
</html>
