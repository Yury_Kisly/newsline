<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
    <head>
    </head>
    <body>

    <spring:url value="/tags" var="url_add"/>

    <c:forEach var="tag" items="${tagList}">

        <spring:url value="/tags/${tag.tagId}" var="url_update"/>
        <spring:url value="/tags/${tag.tagId}" var="url_delete"/>

        <div class="tagLine">
            <div class="tag-tag"> <spring:message code="tag.tag"/> </div>
                <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
            <form:form action="${url_update}"  commandName="tag" method="PUT" cssClass="btn-group-edit">
                <form:input path="tagId" value="${tag.tagId}" type="text" style="display:none" />
                <form:input path="name" value="${tag.name}" class="tag-text-line" type="text"
                            id="tagName${tag.tagId}" disabled="true" />
                <!-- edit tag -->
                <div class="btn-group-edit">
                    <input type="button" id="editTag${tag.tagId}"
                           value="<spring:message code="tag.btn.edit"/>" onclick="edit(${tag.tagId})" />
                </div>
                <!-- update tag -->
                <div class="btn-group-edit" >
                    <input type="submit" id="updateTag${tag.tagId}" hidden="true"
                           value="<spring:message code="tag.btn.upd"/>"/>
                </div>
            </form:form>

            <div class="btn-group-edit" >
                <form:form action="${url_delete}" method="DELETE">
                    <input type="submit" id="deleteTag${tag.tagId}" hidden="true"
                           value="<spring:message code="tag.btn.delete"/>"/>
                </form:form>
            </div>
            <!-- cancel tag -->
            <div class="btn-group-edit">
                <input type="button" id="cancelTag${tag.tagId}"
                       onclick="cancel(${tag.tagId})" hidden="true" value="<spring:message code="tag.btn.cancel"/>"/>
            </div>
        </div>
    </c:forEach>


    <br>

        <!-- form умеет работать с обьектом, используя getter and seter -->
        <!-- берется обьект Tag по модели tag, и методом setName() через form:input="name" задается имя тега -->
        <!--обьект tag передастся в "/create-tag" -->
    <div class="tagLine">
        <div class="tag-tag"> <spring:message code="tag.add-tag" /> </div>
        <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
        <form:form action="${url_add}" method="POST" commandName="tag" cssClass="btn-group-edit" >
            <form:input path="name" type="text" class="tag-text-line" />
            <div class="btn-group-edit">
                <input type="submit" name="btn" value=" <spring:message code="tag.btn.save"/> ">
            </div>
            <form:errors path="name" cssClass="error" style="color:red;"/>
        </form:form>
    </div>

    </body>
</html>
