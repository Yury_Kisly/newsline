<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <div class="login">

            <%--<!-- /login?error=true -->--%>
            <c:if test="${param.error == 'true'}">
                <div style="color:red;margin:10px 0px;">

                    Authentication Failed!<br />
                    Reason :  ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}

                </div>
            </c:if>

            <form name="login-form" action="<c:url value='/j_spring_security_check'/>" method="post" id="login_form">
                <%--this field is used to protect against CSRF (Cross Site Request Forgery)--%>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <table>
                    <tr>
                        <td><spring:message code="login.login"/></td>
                        <td><input type='text' name='username' id='login' placeholder="login" /></td>
                        <%--jquery validation --%>
                        <td><div id="login-error" class="hidden-error">
                            <spring:message code="enter.error.login" />
                        </div></td>
                    </tr>
                    <tr>
                        <td><spring:message code="login.password"/></td>
                        <td><input type='password' name='password' id='pass' placeholder="password"/></td>
                        <%--jquery validation --%>
                        <td><div id="pass-error" class="hidden-error">
                            <spring:message code="enter.error.password"/>
                        </div></td>
                    </tr>
                    <tr>
                        <td><input type="submit" id="login_submit"
                                   value="<spring:message code="button.login.enter"/>" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>
