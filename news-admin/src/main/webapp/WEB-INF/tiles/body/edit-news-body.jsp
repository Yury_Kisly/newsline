<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
    <body>
    <spring:url value="/news-list/edit/${editNewsTO.news.newsId}" var="url_edit"/>
    <div class="field" style="margin: 20px;"><spring:message code="news.edit.message"/> </div>

    <!--оptimistic Lock message-->
    <div style="color:red;">${оptimisticLockException}</div>

    <%-- no need using _csrf.token, because spring form contains it inside itself --%>
    <form:form action="${url_edit}" modelAttribute="editNewsTO" method="post">

        <form:hidden path="news.newsId" value="${editNewsTO.news.newsId}"/>
        <form:hidden path="news.creationDate" value="${editNewsTO.news.creationDate}"/>
        <form:hidden path="news.modificationDate" value="${editNewsTO.news.modificationDate}"/>
        <form:hidden path="news.version" value="${editNewsTO.news.version}"/>
        <%-- Title --%>
        <div class="field">
            <div class="field-name"><spring:message code="news.new.title" /></div>
            <form:input path="news.title" value="${editNewsTO.news.title}" class="field-value"/>
            <form:errors path="news.title" cssClass="error" style="color:red;"/>
        </div>
        <!--Brief:-->
        <div class="field">
            <div class="field-name"><spring:message code="news.new.brief" /></div>
            <form:textarea path="news.shortText" rows="4" value="${editNewsTO.news.shortText}" class="field-value" />
            <form:errors path="news.shortText" cssClass="error" style="color:red;"/>
        </div>
        <!--Content:-->
        <div class="field">
            <div class="field-name"><spring:message code="news.new.content" /></div>
            <form:textarea path="news.fullText" rows="8" value="${editNewsTO.news.fullText}" class="field-value"/>
            <form:errors path="news.fullText" cssClass="error" style="color:red;"/>
        </div>


        <div class="search-criteria">
            <div id="list2" style="position: absolute; background: white;" class="dropdown-check-list2" >
                <span class="anchor2"><spring:message code="select.author"/></span>
                <ul class="items">
                    <li style="text-align: left;">
                        <!-- Author list id -->
                        <form:checkboxes path="authorsId" multiple="multiple" items="${allAuthors}"
                                         itemLabel="authorName" itemValue="authorId" />
                    </li>
                </ul>
            </div>

            <div id="list" style="position: absolute; background: white;" class="dropdown-check-list" >
                <span class="anchor"><spring:message code="select.tag"/></span>
                <ul class="items">
                    <li style="text-align: left;">
                        <!-- Tag list id -->
                        <form:checkboxes path="tagsId" multiple="multiple" items="${allTags}"
                                         itemLabel="name" itemValue="tagId" />
                    </li>
                </ul>
            </div>
            <input type="submit" value="<spring:message code="news.btn.edit"/>" style="margin-left: 400px"/>
            <form:errors path="authorsId" cssClass="error" style="color:red;"/>
        </div>
    </form:form>

    <script type="text/javascript">
        var checkList = document.getElementById('list');
        checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (checkList.classList.contains('visible'))
                checkList.classList.remove('visible');
            else
                checkList.classList.add('visible');
        }

        var checkList2 = document.getElementById('list2');
        checkList2.getElementsByClassName('anchor2')[0].onclick = function (evt) {
            if (checkList2.classList.contains('visible'))
                checkList2.classList.remove('visible');
            else
                checkList2.classList.add('visible');
        }
    </script>

    </body>
</html>
