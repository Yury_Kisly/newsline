<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title></title>
</head>
<body>
    <spring:url value="/news-list/filter" var="url_filter"/>
    <spring:url value="/news-list/reset" var="url_reset"/>
    <spring:url value="/news-list/delete" var="url_delete"/>

    <%--set pattern for date displaying--%>
    <c:set var="datePattern" value="M/d/yyyy"/>

    <div class="search-criteria" >

        <%--&lt;%&ndash; FILTER, RESET &ndash;%&gt;<c:url value='/news-list/filter'/>--%>
        <form action="${url_filter}" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <%-- здесь значение забиратся через request param по name --%>
            <%--<div class="search-criteria">--%>
            <div id="list2" style="position: absolute; background: white;" class="dropdown-check-list2" >
                <span class="anchor2"><spring:message code="select.author"/></span>
                <ul class="items">
                    <li style="text-align: left;">
                        <!-- Author list id -->
                        <form:checkboxes path="authors" items="${authors}" itemLabel="authorName"
                                         itemValue="authorId" name="authorIds"/>
                    </li>
                </ul>
            </div>

            <div id="list" style="position: absolute; background: white;" class="dropdown-check-list" >
                <span class="anchor"><spring:message code="select.tag"/></span>
                <ul class="items">
                    <li style="text-align: left;">
                        <!-- Tag list id -->
                        <form:checkboxes path="tags" items="${tags}" itemLabel="name" itemValue="tagId" name="tagIds"/>
                    </li>
                </ul>
            </div>
            <%--</div>--%>
            <input type="submit" value="<spring:message code="news.btn.filter"/>" style="margin-left: 400px"/>
        </form>

    </div>
    <div class="search-criteria">
        <form action="${url_reset}" method="post">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <input type="submit" value="<spring:message code="news.btn.reset"/>"/>
        </form>
    </div>


    <%-- news list --%>
    <c:choose>
        <c:when test="${wholeNewsList.size() == 0}">
            <spring:message code="news.empty-news-list" />
        </c:when>
        <c:otherwise>


            <form action="${url_delete}" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <c:forEach items="${wholeNewsList}" var="wholeNews">
                    <%-- edit and view news urls for <a href> --%>
                    <spring:url value="/news-list/edit/${wholeNews.news.newsId}" var="editNews" htmlEscape="true"/>
                    <spring:url value="/news-list/view/${wholeNews.news.newsId}" var="viewNews" htmlEscape="true"/>

                    <%--title--%>
                    <div class="title">
                        <a href="${viewNews}"><c:out value="${wholeNews.news.title}"/></a>
                    </div>
                    <%--authors--%>
                    <em>
                        by:
                        <c:forEach items="${wholeNews.authorList}" var="author">
                            ${author.authorName},
                        </c:forEach>
                            <%--date--%>
                        <fmt:formatDate value="${wholeNews.news.creationDate}" pattern="${datePattern}"/>
                    </em>
                    <br>
                    <%--short text--%>
                    <div class="newsLine"> <c:out value="${wholeNews.news.shortText}"/> </div>
                    <%--tags--%>
                    <div class="info-line">
                        <c:forEach items="${wholeNews.tagList}" var="tag">
                            ${tag.name}
                        </c:forEach>
                        <c:out value="Comments(${wholeNews.commentList.size()})" />
                        <a href="${editNews}"><spring:message code="news.btn.edit2"/></a>
                        <input type="checkbox" name="selectedNews" value="${wholeNews.news.newsId}" />
                    </div>
                </c:forEach>
                <div class="info-line"><input type="submit" value="<spring:message code="news.btn.delete"/>" /></div>
            </form>
        </c:otherwise>
    </c:choose>


    <%--PAGINATION--%>
    <spring:url value="/news-list/1" var="url_list1"/>
    <spring:url value="/news-list/${pages}" var="url_list_pages"/>

    <div class="pagin" >
        <c:if test="${currentPage != 1}">
            <a href="${url_list1}"><input type="button" value="<<"/></a>
        </c:if>

        <c:forEach begin="${startPageIndex}" end="${endPageIndex}" var="page">
            <c:choose>
                <c:when test="${currentPage eq page}">
                    <input type="button" value="${page}" class="activePage"/>
                </c:when>
                <c:otherwise>
                    <a href="<spring:url value="/news-list/${page}"/>"><input type="submit" value="${page}"/></a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage != pages}">
            <a href="${url_list_pages}"> <input type="button" value=">>"/> </a>
        </c:if>
    </div>

    <script type="text/javascript">
        var checkList = document.getElementById('list');
        checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (checkList.classList.contains('visible'))
                checkList.classList.remove('visible');
            else
                checkList.classList.add('visible');
        }

        var checkList2 = document.getElementById('list2');
        checkList2.getElementsByClassName('anchor2')[0].onclick = function (evt) {
            if (checkList2.classList.contains('visible'))
                checkList2.classList.remove('visible');
            else
                checkList2.classList.add('visible');
        }
    </script>

</body>
</html>
