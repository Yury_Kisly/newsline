<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <spring:url value="/add-news" var="url_add"/>
        <div class="field" style="margin: 20px;">CREATE NEWS</div>

        <%-- no need to use _csrf.token, because spring form contains it inside itself --%>
        <form:form action="${url_add}" modelAttribute="newsTO" method="post">
            <!--Title:-->
            <div class="field">
                <div class="field-name">
                    <spring:message code="news.new.title" />
                </div>
                <form:input path="news.title" class="field-value"/>
                <form:errors path="news.title" cssClass="error" style="color:red;"/>
            </div>
            <!--Date:-->
            <div class="field">
                <div class="field-name">
                    <spring:message code="news.new.date" />
                </div>
                <input type="text" class="field-value"/>
            </div>
            <!--Brief:-->
            <div class="field">
                <div class="field-name">
                    <spring:message code="news.new.brief" />
                </div>
                <form:textarea path="news.shortText" rows="4" placeholder="short text" class="field-value"/>
                <form:errors path="news.shortText" cssClass="error" style="color:red;"/>
            </div>
            <!--Content:-->
            <div class="field">
                <div class="field-name">
                    <spring:message code="news.new.content" />
                </div>
                <form:textarea path="news.fullText" rows="8" placeholder="full text" class="field-value"/>
                <form:errors path="news.fullText" cssClass="error" style="color:red;"/>
            </div>


            <div class="search-criteria">
                <div id="list2"  class="dropdown-check-list2" >
                    <span class="anchor2"><spring:message code="select.author"/></span>
                    <ul class="items">
                        <li>
                            <!-- Author list id -->
                            <c:forEach items="${allAuthors}" var="author">
                                <c:if test="${empty author.expired}">
                                    <form:checkbox path="authorsId"
                                                   value="${author.authorId}" /> ${author.authorName}<br/>
                                </c:if>
                            </c:forEach>
                        </li>
                    </ul>
                </div>

                <div id="list" class="dropdown-check-list" >
                    <span class="anchor"><spring:message code="select.tag"/></span>
                    <ul class="items">
                        <li>
                            <!-- Tag list id -->
                            <c:forEach items="${allTags}" var="tag">
                                <form:checkbox path="tagsId"
                                               value="${tag.tagId}" /> ${tag.name}<br/>
                            </c:forEach>
                        </li>
                    </ul>
                </div>

                <input type="submit" value="<spring:message code="author.btn.save"/> " style="margin-left: 400px" />
                <form:errors path="authorsId" cssClass="error" style="color:red;"/>
            </div>



        </form:form>


        <script type="text/javascript">
            var checkList = document.getElementById('list');
            checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
                if (checkList.classList.contains('visible'))
                    checkList.classList.remove('visible');
                else
                    checkList.classList.add('visible');
            }

            var checkList2 = document.getElementById('list2');
            checkList2.getElementsByClassName('anchor2')[0].onclick = function (evt) {
                if (checkList2.classList.contains('visible'))
                    checkList2.classList.remove('visible');
                else
                    checkList2.classList.add('visible');
            }
        </script>

    </body>
</html>
