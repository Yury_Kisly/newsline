<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<footer>
    <spring:message code="footer.copyright"/>
</footer>
