<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<header>

    <spring:url value="/resources/image/eng_flag.png" var="ENG"/>
    <spring:url value="/resources/image/rus_flag.png" var="RUS"/>

    <div class="portal-title">
      <spring:message code="header.title"/>
    </div>

    <div class="hello">
        <security:authorize access="isAuthenticated()">
            <spring:message code="heder.hello"/> <security:authentication property="principal" /><%--${admin-name}--%>
        </security:authorize>
    </div>

    <div class="btn-logout">
        <security:authorize access="isAuthenticated()">
            <form action="<c:url value='/j_spring_security_logout'/>" method="post">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <input type="submit" value="<spring:message code="button.header.logout"/> "/>
            </form>
        </security:authorize>
    </div>

    <security:authorize access="isAuthenticated()">
        <div class="language-selector">
            <a href="?lang=en"><img src="${ENG}"/></a>
            <a href="?lang=ru"><img src="${RUS}"/></a>
        </div>
    </security:authorize>

</header>
