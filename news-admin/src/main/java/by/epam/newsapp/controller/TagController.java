package by.epam.newsapp.controller;

import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICompletedTagService;
import by.epam.newsapp.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/tags")
public class TagController {
    private static final String REDIRECT_TAG = "redirect:/tags";
    private static final String TAG_DEFINITION = "tag-definition";
    private static final String TAG_ATTRIBUTE = "tag";
    private static final String TAG_MODEL_ATTR = "tagList";
    private static final String BINDING_ATTRIBUTE = "org.springframework.validation.BindingResult.tag";

    @Autowired
    private ITagService tagService;

    @Autowired
    private ICompletedTagService completedTagService;

    @Autowired
    @Qualifier("tagValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String showTagPage(Model model) throws ServiceException {
        model.addAttribute(TAG_MODEL_ATTR, tagService.readAll());
        //if you create a new instance of that modelAttribute for the redirected page,
        // you will lose the validation errors
        if (!model.containsAttribute(TAG_ATTRIBUTE)) {
            model.addAttribute(TAG_ATTRIBUTE, new Tag());
        }
        return TAG_DEFINITION;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createTag(@ModelAttribute(TAG_ATTRIBUTE) @Validated Tag tag, BindingResult result,
                            final RedirectAttributes redirectAttributes) throws ServiceException {
        if (!result.hasErrors()) {
            tagService.create(tag);
        } else {
            //removed once the next redirected request gets fulfilled
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute(TAG_ATTRIBUTE, tag);
        }
        return REDIRECT_TAG;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String updateTag(@ModelAttribute(TAG_ATTRIBUTE) @Validated Tag tag, BindingResult result,
                                  final RedirectAttributes redirectAttributes) throws ServiceException {

        if (!result.hasErrors()) {
            tagService.update(tag);
        } else {
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute(TAG_ATTRIBUTE, tag);
        }
        return REDIRECT_TAG;
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.DELETE)
    public String deleteTag(@PathVariable("id") Long tagId) throws ServiceException {
        completedTagService.deleteWholeTag(tagId);
        return REDIRECT_TAG;
    }
}
