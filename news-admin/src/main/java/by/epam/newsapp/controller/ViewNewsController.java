package by.epam.newsapp.controller;


import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IAuthorService;
import by.epam.newsapp.service.ICompletedNewsService;
import by.epam.newsapp.service.INewsService;
import by.epam.newsapp.service.ITagService;
import by.epam.newsapp.utils.NewsNavigator;
import by.epam.newsapp.utils.NewsPagination;
import by.epam.newsapp.utils.PageNavigator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ViewNewsController {
    private static final int PAGES_BAR_SHIFT = 8;

    @Autowired
    private IAuthorService authorService;

    @Autowired
    private ITagService tagService;

    @Autowired
    private INewsService newsService;

    @Autowired
    private ICompletedNewsService completedNewsService;

    @RequestMapping(value = "/news-list/{page}", method = RequestMethod.GET)
    public String showNewsList(@PathVariable("page") int pageNumber, Model model,
                                            HttpSession httpSession) throws ServiceException {
        //search criteria should be stored in a session
        SearchCriteria criteria = (SearchCriteria)httpSession.getAttribute("searchCriteria");

        //news ids that would be displayed on a jsp page
        List<Long> listNewsId = newsService.takeBySearchCriteria(criteria);

        //list contains only news for one page according to page number
        List<Long> listOnPage = NewsPagination.getNewsOnPage(listNewsId, pageNumber);
        List<NewsTO> list = new ArrayList<>();

        if (listOnPage != null) {
            for (Long id : listOnPage) {
                list.add(completedNewsService.readWholeNews(id));
            }
        }

        //calc how many pages will be
        int pages;
        if (listNewsId != null) {
            pages = NewsPagination.calcPaginationPages(listNewsId.size());
        } else {
            pages = 1;
        }

        //calc a range for page bar
        int startIndex = PageNavigator.calcStartPageShift(pageNumber, pages, PAGES_BAR_SHIFT);
        int endIndex = PageNavigator.calcEndPageShift(pageNumber, pages, PAGES_BAR_SHIFT);

        model.addAttribute("wholeNewsList", list);
        model.addAttribute("authors", authorService.readAll());
        model.addAttribute("tags", tagService.readAll());
        model.addAttribute("startPageIndex", startIndex);
        model.addAttribute("endPageIndex", endIndex);
        model.addAttribute("pages", pages);
        model.addAttribute("currentPage", pageNumber);

        return "news-list-definition";
    }

    @RequestMapping(value="/news-list/filter", method = RequestMethod.POST)
    public String filterNews(@RequestParam(value = "authorIds", required = false) Long[] authorsId,
                            @RequestParam(value = "tagIds", required = false) Long[] tagsId, HttpSession httpSession) {
        // search criteria would be null until tags or authors wouldn't be choose
        //it was made for method "showNewsList()"
        SearchCriteria searchCriteria = null;
        if (authorsId != null) {
            searchCriteria = new SearchCriteria();
            for(Long id : authorsId) {
                searchCriteria.setAuthorId(id);
            }
        }

        if (tagsId != null) {
            if (searchCriteria == null) {
                searchCriteria = new SearchCriteria();
            }
            for(Long id : tagsId) {
                searchCriteria.setTagId(id);
            }
        }
        httpSession.setAttribute("searchCriteria", searchCriteria);
        return "redirect:/news-list/1";
    }

    //remove search criteria from session
    @RequestMapping(value="/news-list/reset", method = RequestMethod.POST)
    public String resetFilter(HttpSession httpSession) {
        httpSession.setAttribute("searchCriteria", null);
        return "redirect:/news-list/1";
    }

    //delete a list of news marked by checkboxes
    @RequestMapping(value="/news-list/delete", method = RequestMethod.POST)
    public String deleteNews(@RequestParam(value = "selectedNews", required = false) Long[] newsId) throws ServiceException {
        if (newsId != null) {
            completedNewsService.deleteWholeNews(newsId);
        }
        return "redirect:/news-list/1";
    }

    @RequestMapping(value="/news-list/view/next", method = RequestMethod.POST)
    public String moveNextPage(@RequestParam(value = "newsId") Long newsId, HttpSession httpSession) throws ServiceException {
        //search criteria should be stored in the session
        SearchCriteria criteria = (SearchCriteria)httpSession.getAttribute("searchCriteria");

        //news ids that would be displayed on a jsp page
        List<Long> listNewsId = newsService.takeBySearchCriteria(criteria);
        Long nextNewsId = NewsNavigator.takeNextNewsId(listNewsId, newsId);
        return ("redirect:/news-list/view/" + nextNewsId);
    }

    @RequestMapping(value="/news-list/view/prev", method = RequestMethod.POST)
    public String movePrevPage(@RequestParam(value = "newsId") Long newsId, HttpSession httpSession) throws ServiceException {
        //search criteria should be stored in the session
        SearchCriteria criteria = (SearchCriteria)httpSession.getAttribute("searchCriteria");

        //news ids that would be displayed on a jsp page
        List<Long> listNewsId = newsService.takeBySearchCriteria(criteria);
        Long prevNewsId = NewsNavigator.takePrevNewsId(listNewsId, newsId);
        return ("redirect:/news-list/view/" + prevNewsId);
    }


}
