package by.epam.newsapp.controller;


import by.epam.newsapp.entity.CreateNewsTO;
import by.epam.newsapp.entity.News;
import by.epam.newsapp.service.IAuthorService;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICompletedNewsService;
import by.epam.newsapp.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/add-news")
public class AddNewsController {
    private static final String REDIRECT_NEWS_LIST = "redirect:/news-list/1";
    private static final String REDIRECT_NEWS = "redirect:/add-news";
    private static final String NEWS_ATTRIBUTE = "newsTO";
    private static final String TAG_MODEL_ATTR = "allTags";
    private static final String AUTHOR_MODEL_ATTR = "allAuthors";
    private static final String NEWS_DEFINITION = "add-news-definition";
    private static final String BINDING_ATTRIBUTE = "org.springframework.validation.BindingResult.newsTO";

    @Autowired
    private IAuthorService authorService;

    @Autowired
    private ITagService tagService;

    @Autowired
    private ICompletedNewsService newsService;

    @Autowired
    @Qualifier("newsValidator")
    private Validator validator;

    // HandlerMapping decides which controller's method needs to invoke. It depends on the path (URL)
    @RequestMapping(method = RequestMethod.GET)
    public String showAddNewsPage(Model model) throws ServiceException {

        //if you create a new instance of that modelAttribute for the redirected page,
        // you will lose the validation errors
        if (!model.containsAttribute(NEWS_ATTRIBUTE)) {
            CreateNewsTO newsTO = new CreateNewsTO();
            newsTO.setNews(new News());
            model.addAttribute(NEWS_ATTRIBUTE, newsTO);
        }
        model.addAttribute(AUTHOR_MODEL_ATTR, authorService.readAll());
        model.addAttribute(TAG_MODEL_ATTR, tagService.readAll());
        return NEWS_DEFINITION;
    }

    @RequestMapping(method=RequestMethod.POST)
    public String saveNewNews(@ModelAttribute(NEWS_ATTRIBUTE) CreateNewsTO newsTO, BindingResult result,
                                    final RedirectAttributes redirectAttributes) throws ServiceException {
        String view;
        validator.validate(newsTO, result);
        if (!result.hasErrors()) {
            newsService.createWholeNews(newsTO.getNews(), newsTO.getAuthorsId(), newsTO.getTagsId());
            view = REDIRECT_NEWS_LIST;
        } else {
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute(NEWS_ATTRIBUTE, newsTO);
            view = REDIRECT_NEWS;
        }
        return view;
    }
}
