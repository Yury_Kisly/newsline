package by.epam.newsapp.controller;

import by.epam.newsapp.entity.CreateNewsTO;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IAuthorService;
import by.epam.newsapp.service.ICompletedNewsService;
import by.epam.newsapp.service.ITagService;
import org.hibernate.StaleObjectStateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.persistence.OptimisticLockException;
import java.util.Map;

@Controller
@RequestMapping(value = "/news-list/edit")
public class EditNewsController {
    private static final String BINDING_ATTRIBUTE = "org.springframework.validation.BindingResult.newsTO";

    @Autowired
    private IAuthorService authorService;

    @Autowired
    private ITagService tagService;

    @Autowired
    private ICompletedNewsService completedNewsService;

    @Autowired
    @Qualifier("newsValidator")
    private Validator validator;

    //move to edit news page
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String editNewsPage(@PathVariable("id") Long newsId, Model model) throws ServiceException {
        System.out.println("SHOW CURRENT NEWS FOR EDIT");
        String view;
        NewsTO newsTO = completedNewsService.readWholeNews(newsId); //news taken from db
        //move to edit news page, if the news exists
        if (newsTO != null) {
            if (!model.containsAttribute("editNewsTO")) {
                CreateNewsTO createNewsTO = new CreateNewsTO();
                createNewsTO.setNews(newsTO.getNews());
                model.addAttribute("editNewsTO", createNewsTO);    // news to fill in with data from spring form on jsp
            }

            model.addAttribute("allAuthors", authorService.readAll());
            model.addAttribute("allTags", tagService.readAll());
            view = "edit-news-definition";
        } else {    //if no, show all news again
            view = "redirect:/news-list/1";
        }
        return view;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public String editNews(@PathVariable("id") Long newsId, @ModelAttribute("newsTO") CreateNewsTO newsTO,
                                    BindingResult result, final RedirectAttributes redirectAttributes) throws ServiceException {
        String view;
        validator.validate(newsTO, result);
        if (!result.hasErrors()) {
            //Optimistic Lock
            try {
                completedNewsService.updateWholeNews(newsTO.getNews(), newsTO.getAuthorsId(), newsTO.getTagsId());
                view = "redirect:/news-list/1";
            } catch (StaleObjectStateException | OptimisticLockException exp) {
                redirectAttributes.addFlashAttribute("оptimisticLockException", "Current news is locked by other users!");
                view = "redirect:/news-list/edit/" + newsId;
            }

        } else {
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute("editNewsTO", newsTO);
            view = "redirect:/news-list/edit/" + newsId;
        }
        return view;
    }


}
