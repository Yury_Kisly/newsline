package by.epam.newsapp.controller;

import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICommentService;
import by.epam.newsapp.service.ICompletedNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(value = "/news-list/view")
public class CommentsController {
    private static final String REDIRECT_COMMENT = "redirect:/news-list/view/";
    private static final String REDIRECT_NEWS_LIST = "redirect:/news-list/1";
    private static final String COMMENT_DEFINITION = "view-news-definition";
    private static final String COMMENT_ATTRIBUTE = "comment";
    private static final String BINDING_ATTRIBUTE = "org.springframework.validation.BindingResult.comment";

    @Autowired
    private ICommentService commentService;

    @Autowired
    private ICompletedNewsService completedNewsService;

    @Autowired
    @Qualifier("commentValidator")
    private Validator validator;

    //show current news with comments
    @RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
    public String viewNews(@PathVariable Long newsId, Model model) throws ServiceException {
        String view;
        NewsTO newsTO = completedNewsService.readWholeNews(newsId);
        //view news if it exists
        if (newsTO != null) {
            if(!model.containsAttribute(COMMENT_ATTRIBUTE)) {
                model.addAttribute(COMMENT_ATTRIBUTE, new Comment());
            }
            model.addAttribute("currentNews", newsTO);
            view = COMMENT_DEFINITION;
        } else {    //if no, show all news again
            view = REDIRECT_NEWS_LIST;
        }
        return view;
    }

    //post new comment
    @RequestMapping(value="/{newsId}/comment", method={RequestMethod.GET, RequestMethod.POST})
    public String addNewComment(@ModelAttribute(COMMENT_ATTRIBUTE) Comment comment,
                                      @PathVariable Long newsId, BindingResult result,
                                      final RedirectAttributes redirectAttributes) throws ServiceException {
        validator.validate(comment, result);
        if (!result.hasErrors()) {
            commentService.create(comment);
        } else {
            //removed once the next redirected request gets fulfilled
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute(COMMENT_ATTRIBUTE, comment);
        }
        return REDIRECT_COMMENT + newsId;
    }

    //delete comment
    @RequestMapping(value = "/{newsId}/comment/{id}", method = RequestMethod.DELETE)
    public String deleteComment(@PathVariable Long newsId, @PathVariable Long id) throws ServiceException {
        commentService.delete(id);
        return REDIRECT_COMMENT + newsId;
    }



}
