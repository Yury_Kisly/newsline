package by.epam.newsapp.controller;


import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping(value = "/authors")
public class AuthorController {
    private static final String REDIRECT_AUTHOR = "redirect:/authors";
    private static final String AUTHOR_DEFINITION = "author-definition";
    private static final String AUTHOR_ATTRIBUTE = "author";
    private static final String AUTHOR_MODEL_ATTR = "authorList";
    private static final String BINDING_ATTRIBUTE = "org.springframework.validation.BindingResult.author";

    @Autowired
    private IAuthorService authorService;

    @Autowired
    @Qualifier("authorValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(validator);
    }

    @RequestMapping(method= RequestMethod.GET)
    public String showAuthorPage(Model model) throws ServiceException {
        model.addAttribute(AUTHOR_MODEL_ATTR, authorService.readAll());
        if (!model.containsAttribute(AUTHOR_ATTRIBUTE)) {
            model.addAttribute(AUTHOR_ATTRIBUTE, new Author());
        }
        return AUTHOR_DEFINITION;
    }

    @RequestMapping(method=RequestMethod.POST)
    public String createAuthor(@ModelAttribute(AUTHOR_ATTRIBUTE) @Validated Author author, BindingResult result,
                               final RedirectAttributes redirectAttributes) throws ServiceException {

        if (!result.hasErrors()) {
            authorService.create(author);
        } else {
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute(AUTHOR_ATTRIBUTE, author);
        }
        return REDIRECT_AUTHOR;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String updateAuthor(@ModelAttribute(AUTHOR_ATTRIBUTE) @Validated Author author, BindingResult result,
                               final RedirectAttributes redirectAttributes) throws ServiceException {

        if (!result.hasErrors()) {
            authorService.update(author);
        } else {
            redirectAttributes.addFlashAttribute(BINDING_ATTRIBUTE, result);
            redirectAttributes.addFlashAttribute(AUTHOR_ATTRIBUTE, author);
        }
        return REDIRECT_AUTHOR;
    }

    @RequestMapping(value = "/{id}/expire", method = RequestMethod.PUT)
    public String expireAuthor(@PathVariable(value = "id") Long authorId) throws ServiceException {
        authorService.authorExpire(authorId);
        return REDIRECT_AUTHOR;
    }

}
