package by.epam.newsapp.exceptions;


import by.epam.newsapp.exception.ServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public ModelAndView handleServiceException(Exception ex) {
        ModelAndView model = new ModelAndView();
        model.addObject("errorMessage", ex.getMessage());
        model.setViewName("error-def");
        return model;
    }
}
