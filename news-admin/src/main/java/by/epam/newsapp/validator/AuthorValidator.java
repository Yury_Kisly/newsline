package by.epam.newsapp.validator;

import by.epam.newsapp.entity.Author;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class AuthorValidator implements Validator {
    @Override
    public boolean supports(Class clazz) {
        return Author.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Author tag = (Author)o;
//        if (tag.getName() == null || tag.getName().isEmpty()) {
//            errors.rejectValue("name", "tag.error.name");
//        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "authorName", "author.error.name");
    }
}
