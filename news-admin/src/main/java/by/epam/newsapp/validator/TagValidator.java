package by.epam.newsapp.validator;

import by.epam.newsapp.entity.Tag;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class TagValidator implements Validator {
    @Override
    public boolean supports(Class clazz) {
        return Tag.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Tag tag = (Tag)o;
//        if (tag.getName() == null || tag.getName().isEmpty()) {
//            errors.rejectValue("name", "tag.error.name");
//        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "tag.error.name");
    }
}
