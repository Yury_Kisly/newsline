package by.epam.newsapp.validator;


import by.epam.newsapp.entity.Comment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CommentValidator implements Validator {
    @Override
    public boolean supports(Class clazz) {
        return Comment.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "commentText", "comment.error.text");
    }
}
