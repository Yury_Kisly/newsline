package by.epam.newsapp.validator;


import by.epam.newsapp.entity.CreateNewsTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class NewsValidator implements Validator {
    @Override
    public boolean supports(Class clazz) {
        return CreateNewsTO.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CreateNewsTO newsTO = (CreateNewsTO)o;
//        System.out.println("Validation");

        if(newsTO.getNews().getTitle() == null || newsTO.getNews().getTitle().isEmpty()) {
            errors.rejectValue("news.title", "news.error.title");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.shortText", "news.error.brief");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "news.fullText", "news.error.content");

        if (newsTO.getAuthorsId()== null || newsTO.getAuthorsId().isEmpty()) {
            errors.rejectValue("authorsId", "news.error.author");
        }
    }

}
