package by.epam.newsapp.authentication;

import by.epam.newsapp.entity.User;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
    private static final String INVALID_LOGIN_MESSAGE = "Invalid login or password!";

    @Autowired
    private IUserService userService;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //name and pass from login form
        String username = authentication.getName();
        String password = (String) authentication.getCredentials();
        User user = null;
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        String md5Password = encoder.encodePassword(password, null);
        //finds if user exists
        try {
            user = userService.checkUser(username, md5Password);
        } catch (ServiceException e) {
            e.printStackTrace();
        }

        if (user == null) {
            throw new BadCredentialsException(INVALID_LOGIN_MESSAGE);
        }

        List<GrantedAuthority> grantedAuths = new ArrayList<>();
        grantedAuths.add(new SimpleGrantedAuthority(user.getRole().getRoleName()));
        return new UsernamePasswordAuthenticationToken(user.getUserName(), password, grantedAuths);
    }

    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
