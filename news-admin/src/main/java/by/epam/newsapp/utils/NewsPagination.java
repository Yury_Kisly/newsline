package by.epam.newsapp.utils;

import java.util.List;

public class NewsPagination {
    private static final int NEWS_ON_PAGE = 5;

    public static int calcPaginationPages(int newsNumber) {
        //calc how many pages would be
        int pages = (int)Math.ceil(newsNumber * 1.0 / NEWS_ON_PAGE);
        return pages;
    }

    public static List<Long> getNewsOnPage(List<Long> newsIds, Integer currentPage) {
        //начальная страница
        List<Long> list = null;

        if (newsIds != null) {
            int start = (currentPage - 1) * NEWS_ON_PAGE;
            int end = start + NEWS_ON_PAGE;

            if (end > newsIds.size()) {
                end = newsIds.size();
            }
            list = newsIds.subList(start, end);
        }


        // fromIndex low endpoint (inclusive) of the subList
        // toIndex high endpoint (exclusive) of the subList
        return list;
    }



}
