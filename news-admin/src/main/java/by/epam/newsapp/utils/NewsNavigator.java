package by.epam.newsapp.utils;


import java.util.List;

public class NewsNavigator {

    // NEXT ->
    public static Long takeNextNewsId(List<Long> listNewsId, Long currentNewsId) {
        int currentNewsIndex = listNewsId.indexOf(currentNewsId);
        return (currentNewsIndex < listNewsId.size()-1 ? listNewsId.get(currentNewsIndex + 1) : listNewsId.get(0));
    }

    // <- PREVIOUS
    public static Long takePrevNewsId(List<Long> newsId, Long currentNewsId) {
        int currentNewsIndex = newsId.indexOf(currentNewsId);
        return (currentNewsIndex > 0 ? newsId.get(currentNewsIndex - 1) : newsId.get(newsId.size()-1));
    }
}
