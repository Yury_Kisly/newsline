package by.epam.newsapp.utils;


public class PageNavigator {
    private static final int START_DEFAULT_INDEX = 1;

    /**
     * calculates the first page number in the page bar
     * @param currentPage
     * @param shift         how many pages should be shifted
     * @return
     */
    public static int calcStartPageShift(int currentPage, int totalPages, int shift) {
        int startIndex;

        if ((currentPage + shift) < totalPages) {
            startIndex = (currentPage - shift) >= START_DEFAULT_INDEX ? currentPage - shift : START_DEFAULT_INDEX;
        } else {
            int value = totalPages - shift * 2;
            startIndex = (currentPage - shift) >= START_DEFAULT_INDEX ? (value >=0 ? totalPages - shift * 2 : START_DEFAULT_INDEX) : START_DEFAULT_INDEX;
        }
        return startIndex;
    }

    /**
     * calculates the last page number in the page bar
     * @param currentPage
     * @param totalPages    all pages value
     * @param shift         how many pages should be shifted
     * @return
     */
    public static int calcEndPageShift(int currentPage, int totalPages, int shift) {
        int lastIndex;

        if ((currentPage - shift) > START_DEFAULT_INDEX) {
            lastIndex = (currentPage + shift) <= totalPages ? currentPage + shift : totalPages;
        } else {
            int value = shift * 2 + 1;
            lastIndex = (currentPage + shift) < totalPages ? (value <= totalPages ? value : totalPages) : totalPages;
        }
        return lastIndex;
    }



}
