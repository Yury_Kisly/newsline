package by.epam.newsapp.angular.controller;

import by.epam.newsapp.entity.Author;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IAuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //This annotation eliminates the need of annotating each method with @ResponseBody.
@RequestMapping(value = "/authors")
public class AuthorController {

    @Autowired
    IAuthorService authorService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Author> showAllAuthors() throws ServiceException {
        return authorService.readAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Author createAuthor(@RequestBody Author author) throws ServiceException {
        Long authorId = authorService.create(author);
        author.setAuthorId(authorId);
        return author;
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.PUT)
    public void updateAuthor(@PathVariable Long id, @RequestBody Author author) throws ServiceException {
        Author findAuthor = authorService.readById(id);
        if (findAuthor != null) {
            authorService.update(author);
        }
    }

    @RequestMapping(value = "/{id}/expire", method = RequestMethod.PUT)
    public void expireAuthor(@PathVariable Long id) throws ServiceException {
        authorService.authorExpire(id);
    }
}
