package by.epam.newsapp.angular.controller;

import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICompletedTagService;
import by.epam.newsapp.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //This annotation eliminates the need of annotating each method with @ResponseBody.
@RequestMapping(value = "/tags")
public class TagController {

    @Autowired
    private ITagService tagService;

    @Autowired
    private ICompletedTagService completedTagService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Tag> showAllTags() throws ServiceException {
        System.out.println("showAllTags()");
        return tagService.readAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Tag createTag(@RequestBody Tag tag) throws ServiceException {
        Long tagId = tagService.create(tag);
        tag.setTagId(tagId);
        return tag;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateTag(@PathVariable Long id, @RequestBody Tag tag) throws ServiceException {
        System.out.println("updateTag()");
        Tag findTag = tagService.readById(id);
        if (findTag != null) {
            tagService.update(tag);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletetag(@PathVariable Long id) throws ServiceException {
        System.out.println("deleteTag()");
        completedTagService.deleteWholeTag(id);
    }

}
