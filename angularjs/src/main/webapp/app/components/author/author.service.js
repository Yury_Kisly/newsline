'use strict';

app.factory('authorService', ['$http', '$q', function($http, $q) {
    var REST_SERVICE_URI = 'http://localhost:8090/angularjs/news-line/authors';      //url for Spring MVC controller

    var factory = {
        fetchAllAuthors: fetchAllAuthors,
        createAuthor: createAuthor,
        updateAuthor: updateAuthor,
        expireAuthor: expireAuthor
    };

    return factory;

    function fetchAllAuthors() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse) {
                console.error('Error while fetching Authors');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function createAuthor(author) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI, author)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse) {
                console.error('Error while creating new Author');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function updateAuthor(author) {
        var deferred = $q.defer();
        var id = author.authorId;
        $http.put(REST_SERVICE_URI + '/' + id, author)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse) {
                console.error('Error while updating Author');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function expireAuthor(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI + '/' + id + '/expire')
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse) {
                console.error('Error while expiring Author');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
}]);