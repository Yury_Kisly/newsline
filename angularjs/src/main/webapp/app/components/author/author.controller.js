app.controller('authorController', ['$scope','authorService', function($scope, authorService){
    var self = this;        //self identifies this
    self.author = {};       //the same as "this.tag"
    self.authors = [];      //empty object
    self.visible = 0;       //button visible property

    fetchAllAuthors();

    function fetchAllAuthors() {
        authorService.fetchAllAuthors()
            .then(
            function(data) {
                self.authors = data;
            },
            function(errResponse) {
                console.error('Error while fetching Authors');
            }
        );
    }

    self.createAuthor = function(author) {
        authorService.createAuthor(author)
            .then(
            function(data) {
                self.authors.push(data);           //update authors in model by pushing created author to array authors
                self.author = {};                  //clear the form
            },
            function(errResponse) {
                console.error('Error while creating Author');
            }
        );
    };

    self.updateAuthor = function(author) {
        authorService.updateAuthor(author)
            .then(
            function(errResponse) {
                console.error('Error while updating Author');
            }
        );
    };

    self.expireAuthor = function(author) {
        var id = author.tagId;
        authorService.expireAuthor(id)
            .then(
            function(data) {
                self.authors.splice(self.authors.indexOf(author), 1);
            },
            function(errResponse) {
                console.error('Error while expiring Author');
            }
        );

    };

    // active/no-active buttons
    self.edit = function(author) {
        self.visible = author.authorId;
    };

    self.cancel = function() {
        self.visible = 0;
    };

    self.allow = function(id) {
        return self.visible === id;
    };
}]);