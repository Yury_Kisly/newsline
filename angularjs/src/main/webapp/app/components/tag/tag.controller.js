app.controller('tagController', ['$scope','tagService', function($scope, tagService){
    var self = this;        //self identifies this
    self.tag = {};          //the same as "this.tag"
    self.tags = [];         //empty object
    self.visible = 0;       //button visible property

    fetchAllTags();

    //after request with status OK(200, ..) you have to update existing model!!!
    //do not do another request to the db for data updating (like read all tags one more)

    function fetchAllTags() {
        tagService.fetchAllTags()
            .then(
                function(data) {
                    self.tags = data;
                },
                function(errResponse) {
                    console.error('Error while fetching Tags');
                }
            );
    }

    self.createTag = function(tag) {
        tagService.createTag(tag)
            .then(
                function(data) {
                    self.tags.push(data);           //update tags in model by pushing created tag to array tags
                    self.tag = {};                  //clear the form
                },
                function(errResponse) {
                    console.error('Error while creating Tag');
                }
            );
    };

    self.updateTag = function(tag) {
        tagService.updateTag(tag)
            .then(
                function(errResponse) {
                    console.error('Error while updating Tag');
                }
            );
    };

    self.deleteTag = function(tag) {
        var tagId = tag.tagId;
        tagService.deleteTag(tagId)
            .then(
                function(data) {
                    self.tags.splice(self.tags.indexOf(tag), 1);
                },
                function(errResponse) {
                    console.error('Error while deleting Tag');
                }
            );

    };

    // active/no-active buttons
    self.edit = function(tag) {
        self.visible = tag.tagId;
    };

    self.cancel = function() {
        self.visible = 0;
    };

    self.allow = function(id) {
        return self.visible === id;
    };
}]);