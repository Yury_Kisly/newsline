'use strict';

app.factory('tagService', ['$http', '$q', function($http, $q) {
    var REST_SERVICE_URI = 'http://localhost:8090/angularjs/news-line/tags';      //url for Spring MVC controller

    var factory = {
        fetchAllTags: fetchAllTags,
        createTag: createTag,
        updateTag: updateTag,
        deleteTag: deleteTag
    };

    return factory;

    function fetchAllTags() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse) {
                    console.error('Error while fetching Tags');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function createTag(tag) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI, tag)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse) {
                    console.error('Error while creating new Tag');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function updateTag(tag) {
        var deferred = $q.defer();
        var id = tag.tagId;
        $http.put(REST_SERVICE_URI + '/' + id, tag)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse) {
                    console.error('Error while updating Tag');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }

    function deleteTag(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI + '/' + id)
            .then(
                function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse) {
                    console.error('Error while deleting Tag');
                    deferred.reject(errResponse);
                }
            );
        return deferred.promise;
    }
}]);