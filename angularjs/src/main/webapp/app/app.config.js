//ngRoute configuration

app.config(['$routeProvider', function($routeProvider) {

    $routeProvider.
        when('/tags', {
            templateUrl: 'app/components/tag/tag.template.html',
            controller: 'tagController'
        }).
        when('/authors', {
            templateUrl: 'app/components/author/author.template.html',
            controller: 'authorController'
        })
        //.
        //otherwise('/authors')
    ;
}]);
