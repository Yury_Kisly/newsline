<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
<head>
    <title></title>
    <fmt:setLocale value="${language}" />
    <fmt:setBundle basename="loc" />        <!-- file name loc.properties -->
</head>
    <body>
    <%--set pattern for date displaying--%>
    <c:set var="datePatternView" value="MM/dd/yyyy"/>

    <a href="<c:url value="/newsServlet?command=show_news_list"/>"><fmt:message key="single.back"/> </a>

    <%--title--%>
    <div class="title">
        <c:out value="${currentNews.news.title}"/>
    </div>
    <%--authors--%>
    <em>
        by:
        <c:forEach items="${currentNews.authorList}" var="author">
            ${author.authorName},
        </c:forEach>
        <fmt:formatDate value="${currentNews.news.creationDate}" pattern="${datePatternView}" />
    </em>
    <br>
    <%--full text--%>
    <div class="newsLine"> <c:out value="${currentNews.news.fullText}"/> </div>
    <%--comments--%>
    <c:forEach items="${currentNews.commentList}" var="comment">
        <div class="comment">

            <div ><fmt:formatDate value="${comment.creationDate}" pattern="${datePatternView}" /> </div>
            <div><c:out value="${comment.commentText}"/></div>

        </div>
    </c:forEach>

    <%--jquery validation --%>
    <div id="comment-error" class="hidden-error">
        <fmt:message key="error.message.comment" />
    </div>

    <%--add comment--%>
    <form action="<c:url value="/newsServlet"/> " method="post">
        <input type="hidden" name="command" value="add_comment" />
        <input type="hidden" name="newsId" value="${currentNews.news.newsId}" id="comment"/>
        <textarea rows="3" cols="105" name="commentText" id="comment"></textarea>
        <%--jquery validation --%>
        <div id="comment-error" class="hidden-error"><fmt:message key="error.message.comment" /></div> <br/>
        <input type="submit" value="<fmt:message key="button.comment"/>" id="comment-submit"/>
    </form>

    <%--PREV NEXT--%>
    <div class="prev">
        <a href="<c:url value="/newsServlet?command=view_news&news_id=${prev}"/> ">
            <fmt:message key="button.prev"/>
        </a>
    </div>
    <div class="next">
        <a href="<c:url value="/newsServlet?command=view_news&news_id=${next}"/> ">
            <fmt:message key="button.next"/>
        </a>
    </div>

    </body>
</html>
