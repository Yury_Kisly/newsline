<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<html>
<head>
    <title></title>
    <fmt:setLocale value="${language}" />
    <fmt:setBundle basename="loc" />        <!-- file name loc.properties -->
</head>
<body>

    <div class="search-criteria" >

        <form action="<c:url value="/newsServlet"/>" method="post">
            <input type="hidden" name="command" value="filter">
            <div id="list2" style="position: absolute; background: white;" class="dropdown-check-list2" >
                <span class="anchor2"><fmt:message key="select.author"/> </span>
                <ul class="items">
                    <li style="text-align: left;">
                        <!-- Author list id -->
                        <c:forEach items="${authors}" var="author">
                            <p><input type="checkbox" name="checkedAuthors" value="${author.authorId}">${author.authorName}</p>
                        </c:forEach>
                    </li>
                </ul>
            </div>

            <div id="list" style="position: absolute; background: white;" class="dropdown-check-list" >
                <span class="anchor"><fmt:message key="select.tag"/> </span>
                <ul class="items">
                    <li style="text-align: left;">
                        <!-- Tag list id -->
                        <c:forEach items="${tags}" var="tag">
                            <p><input type="checkbox" name="checkedTags" value="${tag.tagId}">${tag.name}</p>
                        </c:forEach>
                    </li>
                </ul>
            </div>
            <input type="submit" value=" <fmt:message key="button.filter"/> " style="margin-left: 400px"/>
        </form>
    </div>

    <div class="search-criteria">
        <form action="<c:url value="/newsServlet"/>" method="post">
            <input type="hidden" name="command" value="reset">
            <input type="submit" value=" <fmt:message key="button.reset"/> "/>
        </form>
    </div>

    <%-- news list --%>
    <c:choose>
        <c:when test="${wholeNewsList.size() == 0}">
            NEWS LIST IS EMPTY
        </c:when>
        <c:otherwise>
            <c:forEach items="${wholeNewsList}" var="wholeNews">
                <%-- view news urls for <a href> --%>
                <%--<spring:url value="/news-list/view/${wholeNews.news.newsId}" var="viewNews" htmlEscape="true"/>--%>

                <%--title--%>
                <div class="title">
                    <c:out value="${wholeNews.news.title}"/>
                </div>
                <%--authors--%>
                <em>
                    by:
                    <c:forEach items="${wholeNews.authorList}" var="author">
                        ${author.authorName},
                    </c:forEach>
                        <%--date--%>
                    <fmt:formatDate value="${wholeNews.news.creationDate}" pattern="${datePattern}"/>
                </em>
                <br>
                <%--short text--%>
                <div class="newsLine"> <c:out value="${wholeNews.news.shortText}"/> </div>
                <%--tags--%>
                <div class="info-line">
                    <c:forEach items="${wholeNews.tagList}" var="tag">
                        ${tag.name}
                    </c:forEach>
                    <c:out value="Comments(${wholeNews.commentList.size()})" />
                    <a href="<c:url value="/newsServlet?command=view_news&news_id=${wholeNews.news.newsId}"/>">edit</a>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>

    <%--PAGINATION--%>
    <div class="pagin" >
        <c:if test="${currentPage != 1}">
            <a href="<c:url value="/newsServlet?command=show_news_list&page=1"/>">
                <input type="button" value="<<"/></a>
        </c:if>

        <c:forEach begin="${startPageIndex}" end="${endPageIndex}" var="page">
            <c:choose>
                <c:when test="${currentPage eq page}">
                    <input type="button" value="${page}" class="activePage"/>
                </c:when>
                <c:otherwise>
                    <a href="<c:url value="/newsServlet?command=show_news_list&page=${page}"/>">
                        <input type="submit" value="${page}"/></a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage != pages}">
            <a href="<c:url value="/newsServlet?command=show_news_list&page=${pages}"/>">
                <input type="button" value=">>"/></a>
        </c:if>
    </div>

    <script type="text/javascript">
        var checkList = document.getElementById('list');
        checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
            if (checkList.classList.contains('visible'))
                checkList.classList.remove('visible');
            else
                checkList.classList.add('visible');
        }

        var checkList2 = document.getElementById('list2');
        checkList2.getElementsByClassName('anchor2')[0].onclick = function (evt) {
            if (checkList2.classList.contains('visible'))
                checkList2.classList.remove('visible');
            else
                checkList2.classList.add('visible');
        }
    </script>

</body>
</html>
