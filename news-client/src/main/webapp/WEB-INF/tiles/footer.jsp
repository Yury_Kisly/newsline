<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<footer>

    <%--<fmt:setLocale value="${language}" />--%>
    <fmt:setBundle basename="loc" />        <!-- file name loc.properties -->
    <div>
        <fmt:message key="footer.coppy" />
    </div>

</footer>
