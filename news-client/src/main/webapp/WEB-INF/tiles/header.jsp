<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<header>
    <fmt:setLocale value="${language}" />
    <fmt:setBundle basename="loc" />        <!-- file name loc.properties -->

    <div class="portal-title">
        <fmt:message key="header.title" />
    </div>

    <div class="language-selector">

        <a href="<c:url value="/newsServlet?command=change_language&language=en"/> ">
            <fmt:message key="header.en" />
        </a>
        <a href="<c:url value="/newsServlet?command=change_language&language=ru"/> ">
            <fmt:message key="header.ru" />
        </a>
    </div>

</header>
