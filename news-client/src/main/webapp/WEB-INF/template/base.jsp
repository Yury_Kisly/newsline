<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>
<head>
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/dropdown.css" type="text/css" />
    <title></title>
</head>
    <body>
        <div id="header"> <tiles:insertAttribute name="header" /> </div>
        <div id="body">   <tiles:insertAttribute name="body" />   </div>
        <div id="footer"> <tiles:insertAttribute name="footer" /> </div>
    </body>
</html>
