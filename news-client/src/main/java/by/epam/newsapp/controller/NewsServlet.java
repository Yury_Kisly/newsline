package by.epam.newsapp.controller;

import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.constant.ProjectConst;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.PropertyManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class NewsServlet extends HttpServlet {
    private static final String ATTR_LAST_PAGE = "lastPage";
    private static final String REDIRECT = "redirect";
    private static final String COMMAND = "command";
    private static final String ERROR_MES = "errorMes";
    private static final String PAGE_ERROR = "path.page.error";

    private ApplicationContext context ;

    @Override
    public void init() throws ServletException {
        super.init();
        context = new ClassPathXmlApplicationContext("classpath*:/appContext.xml"); //Application Context initialize
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("POST");
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("GET");
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = null;

        String action = request.getParameter(COMMAND);
        System.out.println("COMMAND=" + action);

        if (action != null && !action.isEmpty()) {
            ICommand command = (ICommand)context.getBean(action);
            try {
                page = command.execute(request);
            } catch (ServiceException exp) {
                request.setAttribute(ERROR_MES, exp.getMessage());
                page = new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_ERROR);
            }
        }

        //for locale memorize last page
        request.getSession().setAttribute(ATTR_LAST_PAGE, page);

        if (page != null) {
            //PFR pattern
            if (request.getAttribute(REDIRECT) != null) {
                response.sendRedirect(page);
            } else {
                request.getRequestDispatcher(page).forward(request, response);
            }
        } else {
            response.sendRedirect(new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_ERROR));
        }
    }
}
