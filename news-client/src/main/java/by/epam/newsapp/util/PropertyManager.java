package by.epam.newsapp.util;


import java.util.ResourceBundle;

public class PropertyManager {
    private ResourceBundle bundle;

    public PropertyManager(String baseName) {
        bundle = ResourceBundle.getBundle(baseName);
    }

    //take info from .property by key
    public String getProperty(String key) {
        return bundle.getString(key);
    }
}
