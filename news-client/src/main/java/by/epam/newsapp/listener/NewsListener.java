package by.epam.newsapp.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener()
public class NewsListener implements HttpSessionListener {
    private static final String LOCALE = "language";
    private static final String DEFAULT_LOCALE = "en";


    // Public constructor is required by servlet spec
    public NewsListener() {
    }

    // -------------------------------------------------------
    // HttpSessionListener implementation
    // -------------------------------------------------------
    public void sessionCreated(HttpSessionEvent se) {
      /* Session is created. */
        se.getSession().setAttribute(LOCALE, DEFAULT_LOCALE);
    }

    public void sessionDestroyed(HttpSessionEvent se) {
      /* Session is destroyed. */
    }
}
