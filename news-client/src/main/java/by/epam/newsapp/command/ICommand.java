package by.epam.newsapp.command;


import by.epam.newsapp.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;

public interface ICommand {
    String execute(HttpServletRequest request) throws ServiceException;
}
