package by.epam.newsapp.command.impl;

import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.constant.ProjectConst;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.PropertyManager;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component(value = "filter")
public class FilterCommand implements ICommand {
    private static final String CRITERIA = "searchCriteria";
    private static final String CHECKED_AUTHORS = "checkedAuthors";
    private static final String CHECKED_TAGS = "checkedTags";
    private static final String PAGE_INDEX = "path.page.index";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        PropertyManager propertyManager = new PropertyManager(ProjectConst.CONFIG);
        //if session dead
        if (request.getSession(false) == null) {
            return propertyManager.getProperty(PAGE_INDEX);
        }

        // search criteria would be null until tags or authors wouldn't be choose
        SearchCriteria searchCriteria = null;

        //gets all values separated by comma.
        String[] authorsId = request.getParameterValues(CHECKED_AUTHORS);
        String[] tagsId = request.getParameterValues(CHECKED_TAGS);

        if (authorsId != null) {
            searchCriteria = new SearchCriteria();
            for(String id : authorsId) {
                searchCriteria.setAuthorId(Long.parseLong(id));
            }
        }

        if (tagsId != null) {
            if (searchCriteria == null) {
                searchCriteria = new SearchCriteria();
            }
            for(String id : tagsId) {
                searchCriteria.setTagId(Long.parseLong(id));
            }
        }

        request.getSession().setAttribute(CRITERIA, searchCriteria);

        return propertyManager.getProperty(PAGE_INDEX);
    }
}
