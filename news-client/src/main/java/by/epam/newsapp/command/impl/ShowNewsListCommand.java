package by.epam.newsapp.command.impl;


import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.constant.ProjectConst;
import by.epam.newsapp.service.ICompletedNewsService;
import by.epam.newsapp.util.PageNavigator;
import by.epam.newsapp.entity.Author;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.entity.Tag;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.IAuthorService;
import by.epam.newsapp.service.INewsService;
import by.epam.newsapp.service.ITagService;
import by.epam.newsapp.service.impl.CompletedNewsServiceImpl;
import by.epam.newsapp.util.NewsPagination;
import by.epam.newsapp.util.PropertyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;



@Component(value = "show_news_list")
public class ShowNewsListCommand implements ICommand {
    private static final int PAGES_BAR_SHIFT = 8;
    private static final String NEWS_TO_LIST = "wholeNewsList";
    private static final String AUTHOR_LIST = "authors";
    private static final String TAG_LIST = "tags";
    private static final String PAGE = "page";
    private static final String PAGE_PAGIN = "pages";
    private static final String CURRENT_PAGE = "currentPage";
    private static final String CRITERIA = "searchCriteria";
    private static final String QUERY_STRING = "requestQuery";
    private static final String PAGE_NEWS_LIST = "path.page.newslist";
    private static final String PAGE_INDEX = "path.page.index";

    @Autowired
    private INewsService newsService;

    @Autowired
    private IAuthorService authorService;

    @Autowired
    private ITagService tagService;

    @Autowired
    private ICompletedNewsService completedNewsService;


    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        PropertyManager propertyManager = new PropertyManager(ProjectConst.CONFIG);
        //if session dead
        if (request.getSession(false) == null) {
            return propertyManager.getProperty(PAGE_INDEX);
        }

        String pageTx = request.getParameter(PAGE);
        int currentPage = (pageTx != null ? Integer.parseInt(pageTx) : 1); //to determine what page should be shown

        //search criteria should be stored in a session
        SearchCriteria criteria = (SearchCriteria)request.getSession().getAttribute(CRITERIA);

        //news ids that would be displayed on a jsp page
        List<Long> listNewsId = newsService.takeBySearchCriteria(criteria);

        //list contains only news for one page according to page number
        List<Long> listOnPage = NewsPagination.getNewsOnPage(listNewsId, currentPage);
        List<NewsTO> newsList = new ArrayList<>();
        for (Long id : listOnPage) {
            newsList.add(completedNewsService.readWholeNews(id));
        }
        //read tags and authors for checkboxes
        List<Author> authorList = authorService.readAll();
        List<Tag> tagList = tagService.readAll();

        //calc how many pages will be
        int pages;
        if (listNewsId != null) {
            pages = NewsPagination.calcPaginationPages(listNewsId.size());
        } else {
            pages = 1;
        }

        //calc a range for page bar
        int startIndex = PageNavigator.calcStartPageShift(currentPage, pages, PAGES_BAR_SHIFT);
        int endIndex = PageNavigator.calcEndPageShift(currentPage, pages, PAGES_BAR_SHIFT);

        request.setAttribute(NEWS_TO_LIST, newsList);
        request.setAttribute(AUTHOR_LIST, authorList);
        request.setAttribute(TAG_LIST, tagList);
        request.setAttribute(PAGE_PAGIN, pages);
        request.setAttribute(CURRENT_PAGE, currentPage);
        request.setAttribute("startPageIndex", startIndex);
        request.setAttribute("endPageIndex", endIndex);
        //it save a string like "command=show_news_list&page=2"
        request.getSession().setAttribute(QUERY_STRING, request.getQueryString());

        return propertyManager.getProperty(PAGE_NEWS_LIST);
    }
}
