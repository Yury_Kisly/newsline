package by.epam.newsapp.command.impl;

import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.constant.ProjectConst;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.entity.SearchCriteria;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICompletedNewsService;
import by.epam.newsapp.service.INewsService;
import by.epam.newsapp.util.NewsNavigator;
import by.epam.newsapp.util.PropertyManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component(value = "view_news")
public class SingleNewsCommand implements ICommand {
    private static final String PARAM_NEWS_ID = "news_id";
    private static final String NEXT = "next";
    private static final String PREV = "prev";
    private static final String NEWS = "currentNews";
    private static final String CRITERIA = "searchCriteria";
    private static final String QUERY_STRING = "requestQuery";
    private static final String PAGE_SINGLE_LIST = "path.page.singlenews";
    private static final String PAGE_INDEX = "path.page.index";

    @Autowired
    private ICompletedNewsService completedNewsService;

    @Autowired
    private INewsService newsService;

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {

        PropertyManager propertyManager = new PropertyManager(ProjectConst.CONFIG);
        //if session dead
        if (request.getSession(false) == null) {
            return propertyManager.getProperty(PAGE_INDEX);
        }


        String id = request.getParameter(PARAM_NEWS_ID);
        Long newsId = Long.parseLong(id);

        SearchCriteria criteria = (SearchCriteria)request.getSession().getAttribute(CRITERIA);
        //news ids that would be displayed on a jsp page
        List<Long> listNewsId = newsService.takeBySearchCriteria(criteria);
        Long prevNewsId = NewsNavigator.takePrevNewsId(listNewsId, newsId);
        Long nextNewsId = NewsNavigator.takeNextNewsId(listNewsId, newsId);

        NewsTO newsTO = completedNewsService.readWholeNews(newsId);
        //it save a string like "command=show_news_list&page=2"
        request.getSession().setAttribute(QUERY_STRING, request.getQueryString());

        request.setAttribute(NEWS, newsTO);
        request.setAttribute(NEXT, nextNewsId);
        request.setAttribute(PREV, prevNewsId);

        return propertyManager.getProperty(PAGE_SINGLE_LIST);
    }
}
