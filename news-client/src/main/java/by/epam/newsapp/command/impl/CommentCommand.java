package by.epam.newsapp.command.impl;

import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.entity.Comment;
import by.epam.newsapp.entity.NewsTO;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.service.ICommentService;
import by.epam.newsapp.service.ICompletedNewsService;
import by.epam.newsapp.validator.ProjectValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component(value = "add_comment")
public class CommentCommand implements ICommand {
    private static final String PARAM_NEWS_ID = "newsId";
    private static final String PARAM_COMMENT_TEXT = "commentText";
    private static final String NEWS = "currentNews";
    private static final String REDIRECT = "redirect";
    private static final String URL_REDIRECT = "/newsServlet?command=view_news&news_id=";

    @Autowired
    private ICommentService commentService;

    @Autowired
    private ICompletedNewsService completedNewsService;

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String commentText = request.getParameter(PARAM_COMMENT_TEXT);
        String id = request.getParameter(PARAM_NEWS_ID);
        Long newsId = Long.parseLong(id);
        System.out.println("ADD comment prev");
        if (ProjectValidator.checkComment(commentText)) {
            Comment comment = new Comment();
            comment.setNewsId(newsId);
            comment.setCommentText(commentText);
            commentService.create(comment);
            System.out.println("Comment valid");
        }

        NewsTO newsTO = completedNewsService.readWholeNews(newsId);
        request.setAttribute(NEWS, newsTO);
        request.setAttribute(REDIRECT, true);

        return URL_REDIRECT.concat(id);
    }

}
