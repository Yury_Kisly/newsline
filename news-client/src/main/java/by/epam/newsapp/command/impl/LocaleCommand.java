package by.epam.newsapp.command.impl;


import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.exception.ServiceException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component(value = "change_language")
public class LocaleCommand implements ICommand {
    private static final String PARAM_LANGUAGE = "language";
    private static final String QUERY_STRING = "requestQuery";

    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        String lang = request.getParameter(PARAM_LANGUAGE);     //take value(en, ru) by name(language) from request
        HttpSession session = request.getSession();
        session.setAttribute(PARAM_LANGUAGE, lang);//set selected value(en, ru) to a session

        String path = buildPathPage(request);
        return path;
    }

    private String buildPathPage(HttpServletRequest request) {
        //take a string like "command=show_news_list&page=2"
        String requestQuery = (String)request.getSession().getAttribute(QUERY_STRING);
        String servletPath = request.getServletPath();
        StringBuilder bd = new StringBuilder(servletPath);
        bd.append('?');
        bd.append(requestQuery);
        return bd.toString();
    }
}
