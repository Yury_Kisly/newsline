package by.epam.newsapp.command.impl;

import by.epam.newsapp.command.ICommand;
import by.epam.newsapp.constant.ProjectConst;
import by.epam.newsapp.exception.ServiceException;
import by.epam.newsapp.util.PropertyManager;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component(value = "reset")
public class ResetCommand implements ICommand {
    private static final String CRITERIA = "searchCriteria";
    private static final String PAGE_INDEX = "path.page.index";


    @Override
    public String execute(HttpServletRequest request) throws ServiceException {
        request.getSession().removeAttribute(CRITERIA);
        return new PropertyManager(ProjectConst.CONFIG).getProperty(PAGE_INDEX);
    }
}
